/*******************************************************************************
 * FoxDownloadableContentEnumeratorPC generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxDownloadableContentEnumeratorPC extends DownloadableContentEnumerator;

defaultproperties
{
    DLCRootDir="../../FoxGame/DLC/"
}