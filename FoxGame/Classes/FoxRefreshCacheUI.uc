/*******************************************************************************
 * FoxRefreshCacheUI generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxRefreshCacheUI extends FoxUIBaseTask within FoxUI;

var bool bInventoryReadEnabled;
var bool bLoadoutsReadEnabled;
var bool bStoreReadEnabled;
var bool bMailReadEnabled;
var bool bClanReadEnabled;
var bool bUnlockLevelReadEnabled;
var bool bFriendReadEnabled;
var bool bHistoryReadEnabled;
var bool bShowWorkingDialog;
var private bool bInventoryRead;
var private bool bInventoryReadResponse;
var private bool bLoadoutsRead;
var private bool bLoadoutsReadResponse;
var private bool bStoreRead;
var private bool bStoreReadResponse;
var private bool bMailRead;
var private bool bMailReadResponse;
var private bool bClanRead;
var private bool bClanReadResponse;
var bool bUnlockLevelRead;
var bool bUnlockLevelReadResponse;
var private bool bFriendread;
var private bool bFriendReadResponse;
var private bool bHistoryRead;
var private bool bHistoryReadResponse;
var private string FailReason;
var private const localized string LocErrorInventoryAccess;
var private const localized string LocErrorLoadoutsAccess;
var private const localized string LocErrorStoreAccess;
var private const localized string LocErrorMailAccess;
var private const localized string LocErrorClanAccess;
var private const localized string LocErrorFriendAccess;
var private const localized string LocErrorUnlockLevelAccess;
var private const localized string LocErrorHistoryAccess;
var delegate<PreRefreshCache> __PreRefreshCache__Delegate;
var delegate<PostRefreshCache> __PostRefreshCache__Delegate;

delegate PreRefreshCache()
{
    //return;    
}

delegate PostRefreshCache(bool bResult, const string FailureReason)
{
    //return;    
}

function bool RefreshCache()
{
    // End:0x8C
    if(StartTask())
    {
        ConfirmRefreshCacheIntent(((((((bInventoryReadEnabled || bLoadoutsReadEnabled) || bStoreReadEnabled) || bMailReadEnabled) || bClanReadEnabled) || bFriendReadEnabled) || bUnlockLevelReadEnabled) || bHistoryReadEnabled);
        return true;
    }
    return false;
    //return ReturnValue;    
}

protected function NotifyProcessComplete(FoxDialogBoxBase.EDialogBoxConfirmType Selection)
{
    super.NotifyProcessComplete(Selection);
    // End:0x3F
    if(bShowWorkingDialog)
    {
        Outer.CloseDialogBox();
    }
    PostRefreshCache(Selection == 0, FailReason);
    //return;    
}

protected function RegisterOnlineDelegates()
{
    Outer.StoreInterface.AddQueryInventoryCompleteDelegate(0, ReadInventoryComplete);
    Outer.StoreInterface.AddLoadoutQueryDelegate(ReadLoadoutsComplete);
    Outer.StoreInterface.AddQueryStoreCompleteDelegate(0, ReadStoreComplete);
    Outer.StoreInterface.AddQueryMailCompleteDelegate(0, ReadMailComplete);
    Outer.StoreInterface.AddQueryUnlockLevelCompleteDelegate(0, ReadUnlockLevelsComplete);
    Outer.ClanInterface.AddOnGetClanInfo(ReadClanComplete);
    Outer.PlayerInterface.AddReadFriendsCompleteDelegate(0, ReadFriendComplete);
    Outer.StoreInterface.AddQueryHistoryCompleteDelegate(0, ReadHistoryComplete);
    //return;    
}

protected function CleanupOnlineDelegates()
{
    Outer.StoreInterface.ClearQueryInventoryCompleteDelegate(0, ReadInventoryComplete);
    Outer.StoreInterface.ClearLoadoutQueryDelegate(ReadLoadoutsComplete);
    Outer.StoreInterface.ClearQueryStoreCompleteDelegate(0, ReadStoreComplete);
    Outer.StoreInterface.ClearQueryMailCompleteDelegate(0, ReadMailComplete);
    Outer.StoreInterface.ClearQueryUnlockLevelCompleteDelegate(0, ReadUnlockLevelsComplete);
    Outer.ClanInterface.ClearOnGetClanInfo(ReadClanComplete);
    Outer.PlayerInterface.ClearReadFriendsCompleteDelegate(0, ReadFriendComplete);
    Outer.StoreInterface.ClearQueryHistoryCompleteDelegate(0, ReadHistoryComplete);
    //return;    
}

private final function bool ReadInventoryCache()
{
    local FoxItemCacheInventory Cache;

    // End:0x86
    if(class'FoxDataStore_StoreData'.static.GetCachedInventory(Cache))
    {
        bInventoryRead = !Cache.IsDirty();
        return bInventoryRead || Cache.Refresh();
    }
    return false;
    //return ReturnValue;    
}

private final function bool ReadLoadoutsCache()
{
    local FoxItemCacheLoadouts Cache;

    // End:0x86
    if(class'FoxDataStore_StoreData'.static.GetCachedLoadouts(Cache))
    {
        bLoadoutsRead = !Cache.IsDirty();
        return bLoadoutsRead || Cache.Refresh();
    }
    return false;
    //return ReturnValue;    
}

private final function bool ReadStoreCache()
{
    local FoxItemCacheStore Cache;

    // End:0x86
    if(class'FoxDataStore_StoreData'.static.GetCachedStore(Cache))
    {
        bStoreRead = !Cache.IsDirty();
        return bStoreRead || Cache.Refresh();
    }
    return false;
    //return ReturnValue;    
}

private final function bool ReadMailCache()
{
    local FoxItemCacheMail Cache;

    // End:0x86
    if(class'FoxDataStore_StoreData'.static.GetCachedMail(Cache))
    {
        bMailRead = !Cache.IsDirty();
        return bMailRead || Cache.Refresh();
    }
    return false;
    //return ReturnValue;    
}

private final function bool ReadClanCache()
{
    local FoxItemCacheClan Cache;

    // End:0x86
    if(class'FoxDataStore_StoreData'.static.GetCachedClan(Cache))
    {
        bClanRead = !Cache.IsDirty();
        return bClanRead || Cache.Refresh();
    }
    return false;
    //return ReturnValue;    
}

private final function bool ReadFriendCache()
{
    local FoxItemCacheFriends Cache;

    // End:0x86
    if(class'FoxDataStore_StoreData'.static.GetCachedFriends(Cache))
    {
        bFriendread = !Cache.IsDirty();
        return bFriendread || Cache.Refresh();
    }
    return false;
    //return ReturnValue;    
}

private final function bool ReadUnlockLevelCache()
{
    local FoxItemCacheUnlockLevels Cache;

    // End:0x86
    if(class'FoxDataStore_StoreData'.static.GetCachedUnlockLevels(Cache))
    {
        bUnlockLevelRead = !Cache.IsDirty();
        return bUnlockLevelRead || Cache.Refresh();
    }
    return false;
    //return ReturnValue;    
}

private final function bool ReadHistoryCache()
{
    local FoxItemCacheHistory Cache;

    // End:0x86
    if(class'FoxDataStore_StoreData'.static.GetCachedHistory(Cache))
    {
        bHistoryRead = !Cache.IsDirty();
        return bHistoryRead || Cache.Refresh();
    }
    return false;
    //return ReturnValue;    
}

private final function ConfirmRefreshCacheIntent(bool bResult)
{
    local DialogBoxProperties Properties;
    local Engine.OnlineSubsystem.ELoginStatus LoginStatus;
    local UniqueNetId InvalidNetId;

    InvalidNetId.Uid.A = 0;
    InvalidNetId.Uid.B = 0;
    LoginStatus = Outer.PCOwner.OnlineSub.PlayerInterface.GetLoginStatus(0);
    switch(LoginStatus)
    {
        // End:0x5BD
        case 2:
            // End:0x5BD
            if(bResult)
            {
                bInventoryRead = !bInventoryReadEnabled;
                bInventoryReadResponse = !bInventoryReadEnabled;
                bLoadoutsRead = !bLoadoutsReadEnabled;
                bLoadoutsReadResponse = !bLoadoutsReadEnabled;
                bStoreRead = !bStoreReadEnabled;
                bStoreReadResponse = !bStoreReadEnabled;
                bMailRead = !bMailReadEnabled;
                bMailReadResponse = !bMailReadEnabled;
                bClanRead = !bClanReadEnabled;
                bClanReadResponse = !bClanReadEnabled;
                bFriendread = !bFriendReadEnabled;
                bFriendReadResponse = !bFriendReadEnabled;
                bUnlockLevelRead = !bUnlockLevelReadEnabled;
                bUnlockLevelReadResponse = !bUnlockLevelReadEnabled;
                bHistoryRead = !bHistoryReadEnabled;
                bHistoryReadResponse = !bHistoryReadEnabled;
                // End:0x2D9
                if(!bInventoryRead && !ReadInventoryCache())
                {
                    ReadInventoryComplete(Outer.PCOwner.PlayerReplicationInfo.UniqueId, false, "");
                    return;
                }
                // End:0x308
                if(!bLoadoutsRead && !ReadLoadoutsCache())
                {
                    ReadLoadoutsComplete(false, "");
                    return;
                }
                // End:0x336
                if(!bStoreRead && !ReadStoreCache())
                {
                    ReadStoreComplete("");
                    return;
                }
                // End:0x366
                if(!bMailRead && !ReadMailCache())
                {
                    ReadMailComplete(0, "");
                    return;
                }
                // End:0x394
                if(!bClanRead && !ReadClanCache())
                {
                    ReadClanComplete("");
                    return;
                }
                // End:0x3C1
                if(!bFriendread && !ReadFriendCache())
                {
                    ReadFriendComplete(false);
                    return;
                }
                // End:0x3EF
                if(!bUnlockLevelRead && !ReadUnlockLevelCache())
                {
                    ReadUnlockLevelsComplete("");
                    return;
                }
                // End:0x426
                if(!bHistoryRead && !ReadHistoryCache())
                {
                    ReadHistoryComplete(InvalidNetId, "");
                    return;
                }
                // End:0x5BA
                if(((((((!bInventoryRead || !bLoadoutsRead) || !bStoreRead) || !bMailRead) || !bClanRead) || !bFriendread) || !bUnlockLevelRead) || !bHistoryRead)
                {
                    PreRefreshCache();
                    // End:0x5B8
                    if(bShowWorkingDialog)
                    {
                        Properties.DialogType = 0;
                        Properties.TitleText = Outer.LocWorkingLabel;
                        Properties.BodyText = "Refreshing...";
                        Properties.bShowTimer = true;
                        Properties.bCloseOnButtonPress = false;
                        Outer.ShowDialogBox(Properties);
                    }
                    return;
                }
            }
            // End:0x5C0
            else
            {
        // End:0xFFFF
        default:
        }
    NotifyProcessComplete(((LoginStatus >= 1) ? 0 : 1));
    //return;    
}

private final function ConditionalFinishCacheRefresh()
{
    local bool bFailed;

    // End:0x3D
    if(bInventoryReadResponse && !bInventoryRead)
    {
        bFailed = true;
        FailReason = LocErrorInventoryAccess;
    }
    // End:0x7D
    if(bLoadoutsReadResponse && !bLoadoutsRead)
    {
        bFailed = true;
        FailReason = LocErrorLoadoutsAccess;
    }
    // End:0x27F
    else
    {
        // End:0xBD
        if(bStoreReadResponse && !bStoreRead)
        {
            bFailed = true;
            FailReason = LocErrorStoreAccess;
        }
        // End:0x27F
        else
        {
            // End:0xFD
            if(bMailReadResponse && !bMailRead)
            {
                bFailed = true;
                FailReason = LocErrorMailAccess;
            }
            // End:0x27F
            else
            {
                // End:0x13D
                if(bClanReadResponse && !bClanRead)
                {
                    bFailed = true;
                    FailReason = LocErrorClanAccess;
                }
                // End:0x27F
                else
                {
                    // End:0x17D
                    if(bFriendReadResponse && !bFriendread)
                    {
                        bFailed = true;
                        FailReason = LocErrorFriendAccess;
                    }
                    // End:0x27F
                    else
                    {
                        // End:0x1BD
                        if(bUnlockLevelReadResponse && !bUnlockLevelRead)
                        {
                            bFailed = true;
                            FailReason = LocErrorUnlockLevelAccess;
                        }
                        // End:0x27F
                        else
                        {
                            // End:0x1FD
                            if(bHistoryReadResponse && !bHistoryRead)
                            {
                                bFailed = true;
                                FailReason = LocErrorHistoryAccess;
                            }
                            // End:0x27F
                            else
                            {
                                // End:0x27F
                                if(((((((bInventoryRead && bLoadoutsRead) && bStoreRead) && bMailRead) && bClanRead) && bFriendread) && bUnlockLevelRead) && bHistoryRead)
                                {
                                    NotifyProcessComplete(0);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    // End:0x298
    if(bFailed)
    {
        NotifyProcessFailed(0);
    }
    //return;    
}

private final function ReadInventoryComplete(const UniqueNetId ForId, const bool bSuccess, const string InventoryData)
{
    bInventoryRead = bSuccess;
    bInventoryReadResponse = true;
    ConditionalFinishCacheRefresh();
    //return;    
}

private final function ReadLoadoutsComplete(const bool bSuccess, const string LoadoutsListXML)
{
    bLoadoutsRead = true;
    bLoadoutsReadResponse = true;
    // End:0x31
    if(!bSuccess)
    {
        ScriptTrace();
    }
    ConditionalFinishCacheRefresh();
    //return;    
}

private final function ReadStoreComplete(const string StoreData)
{
    bStoreRead = StoreData != "";
    bStoreReadResponse = true;
    ConditionalFinishCacheRefresh();
    //return;    
}

private final function ReadUnlockLevelsComplete(const string UnlockLevelData)
{
    bUnlockLevelRead = UnlockLevelData != "";
    bUnlockLevelReadResponse = true;
    ConditionalFinishCacheRefresh();
    //return;    
}

private final function ReadMailComplete(byte LocalUserNum, string MailData)
{
    bMailRead = MailData != "";
    bMailReadResponse = true;
    ConditionalFinishCacheRefresh();
    //return;    
}

private final function ReadClanComplete(string ClanInfo)
{
    bClanRead = true;
    bClanReadResponse = true;
    FailReason = ClanInfo;
    ConditionalFinishCacheRefresh();
    //return;    
}

private final function ReadFriendComplete(bool Success)
{
    bFriendread = Success;
    bFriendReadResponse = true;
    ConditionalFinishCacheRefresh();
    //return;    
}

private final function ReadHistoryComplete(const UniqueNetId ForId, const string HistoryData)
{
    local UniqueNetId MyId;

    Outer.PlayerInterface.GetUniquePlayerId(0, MyId);
    bHistoryRead = ForId == MyId;
    bHistoryReadResponse = true;
    ConditionalFinishCacheRefresh();
    //return;    
}

defaultproperties
{
    bShowWorkingDialog=true
    LocErrorInventoryAccess="Unable to retrieve inventory data for the logged in player. Please try again later."
    LocErrorLoadoutsAccess="Unable to retrieve loadouts data for the logged in player. Please try again later."
    LocErrorStoreAccess="Unable to retrieve store data for the logged in player. Please try again later."
    LocErrorMailAccess="Unable to retrieve mail for the logged in player. Please try again later."
    LocErrorClanAccess="Unable to retrieve clan info for the logged in player. Please try again later."
    LocErrorFriendAccess="Unable to retrieve friend info for the logged in player. Please try again later."
    LocErrorUnlockLevelAccess="Unable to retrieve unlock level data for the logged in player. Please try again later."
    LocErrorHistoryAccess="Unable to retrieve mission data for the logged in player. Please try again later."
    bBlockSubsequent=false
}