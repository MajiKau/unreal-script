/*******************************************************************************
 * FoxDamageType_MeleeTaunt_Handfire generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxDamageType_MeleeTaunt_Handfire extends FoxDamageType_MeleeTaunt;

defaultproperties
{
    TauntUnlockID=346
    DamagedFFWaveform=ForceFeedbackWaveform'Default__FoxDamageType_MeleeTaunt_Handfire.ForceFeedbackWaveformDamage'
    KilledFFWaveform=ForceFeedbackWaveform'Default__FoxDamageType_MeleeTaunt_Handfire.ForceFeedbackWaveformDeath'
}