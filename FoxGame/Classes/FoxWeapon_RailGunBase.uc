/*******************************************************************************
 * FoxWeapon_RailGunBase generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxWeapon_RailGunBase extends FoxWeapon_SemiAutomaticRifleBase
    abstract
    native(Weapon)
    config(Weapon)
    hidecategories(Navigation);

simulated function CustomFire()
{
    local Vector StartTrace, EndTrace;
    local array<ImpactInfo> PawnImpactList;
    local int Idx;
    local ImpactInfo RealImpact;

    StartTrace = Instigator.GetWeaponStartTraceLocation();
    EndTrace = StartTrace + (vector(GetAdjustedAim(StartTrace)) * (GetTraceRange()));
    RealImpact = CalcRailFire(StartTrace, EndTrace, PawnImpactList, vect(15.0, 15.0, 15.0));
    // End:0xD9
    if(Role == ROLE_Authority)
    {
        SetFlashLocation(RealImpact.HitLocation);
    }
    // End:0x136
    else
    {
        // End:0x136
        if(WorldInfo.GRI.bClientSideHitDetection)
        {
            SetFlashLocation(RealImpact.HitLocation);
        }
    }
    Idx = 0;
    J0x141:
    // End:0x1D4 [Loop If]
    if(Idx < PawnImpactList.Length)
    {
        // End:0x1C6
        if(PawnImpactList[Idx].HitActor != RealImpact.HitActor)
        {
            ServerProcessInstantHit(PawnImpactList[Idx], CurrentFireMode);
        }
        ++ Idx;
        // [Loop Continue]
        goto J0x141;
    }
    ServerProcessInstantHit(RealImpact, CurrentFireMode);
    //return;    
}

// Export UFoxWeapon_RailGunBase::execCalcRailFire(FFrame&, void* const)
native function ImpactInfo CalcRailFire(Vector StartTrace, Vector EndTrace, optional out array<ImpactInfo> ImpactList, optional Vector Extent);

simulated function int FillAmmo()
{
    return AddAmmo(GetMaxSpareAmmoSize());
    //return ReturnValue;    
}

defaultproperties
{
    bIsDepotWeapon=true
    FireFeedbackWaveform=ForceFeedbackWaveform'Default__FoxWeapon_RailGunBase.ForceFeedbackWaveformFire'
    TightAimCameraShake=CameraShake'Default__FoxWeapon_RailGunBase.Shake0'
    NormalCameraShake=CameraShake'Default__FoxWeapon_RailGunBase.Shake1'
    MuzzleFlashLight=PointLightComponent'Default__FoxWeapon_RailGunBase.LightComponent0'
    begin object name=LensComp class=LensFlareComponent
        NextTraceTime=-0.7173070
        ReplacementPrimitive=none
    object end
    // Reference: LensFlareComponent'Default__FoxWeapon_RailGunBase.LensComp'
    MuzzleLensFlare=LensComp
    WeaponFireTypes=/* Array type was not detected. */
    begin object name=WeaponMesh class=FoxFirstPersonSkeletalMeshComponent
        ReplacementPrimitive=none
    object end
    // Reference: FoxFirstPersonSkeletalMeshComponent'Default__FoxWeapon_RailGunBase.WeaponMesh'
    Mesh=WeaponMesh
}