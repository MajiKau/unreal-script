/*******************************************************************************
 * FoxHud_OS generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxHud_OS extends FoxHUDTDM
    transient
    config(UI)
    hidecategories(Navigation);

var const localized string LocStringFirstWave;
var const localized string LocStringFinalWave;
var const localized string LocStringNextWave;
var const localized string LocStringWaveComplete;
var const localized string LocStringFinalWaveComplete;
var const localized string LocStringAllPlayersDead;
var const localized string LocStringMissionFailed;
var const localized string LocStringMissionSucceeded;
var const localized string LocStringMissionFailedDesc;
var const localized string LocStringMIssionFailedTime;
var const localized string LocStringTeammateDown;
var const localized string LocStringDepotChanged;
var const localized string LocStringDepotReboot;
var const localized string LocStringNewObjective;
var const localized string LocStringObjectiveComplete;
var const localized string LocStringObjectiveFailed;

function string GetEventName(FoxTypes.EScoreEvents EventType, optional bool bFriendly)
{
    bFriendly = true;
    switch(EventType)
    {
        // End:0x27
        case 47:
            return LocStringFirstWave;
        // End:0x36
        case 48:
            return LocStringNextWave;
        // End:0x45
        case 49:
            return LocStringFinalWave;
        // End:0x54
        case 51:
            return LocStringWaveComplete;
        // End:0x63
        case 52:
            return LocStringFinalWaveComplete;
        // End:0x72
        case 50:
            return LocStringAllPlayersDead;
        // End:0x81
        case 54:
            return LocStringDepotChanged;
        // End:0x90
        case 55:
            return LocStringDepotReboot;
        // End:0xFFFF
        default:
            return super(FoxHUD).GetEventName(EventType, bFriendly);
    }
    //return ReturnValue;    
}

function NotifyPawnInjured(FoxPawn inPawn)
{
    // End:0x13D
    if((inPawn.InjuredPRI.GetTeamNum() == 0) && PlayerOwner.PlayerReplicationInfo != inPawn.InjuredPRI)
    {
        // End:0x13D
        if((DynamicUI != none) && FoxHudUI_OS(DynamicUI.HUD) != none)
        {
            FoxHudUI_OS(DynamicUI.HUD).PumpDepotPing(inPawn.InjuredPRI.GetHumanReadableName() @ LocStringTeammateDown, false);
        }
    }
    //return;    
}

function NotifyOSGameOver(byte InWinningTeamIndex, int GameEndExperience, int MyPlacement, int inFirstPlaceID, int inSecondPlaceID, int inThirdPlaceID, int inForthPlaceID)
{
    local FoxGRI GRI;

    // End:0x2D
    if(FoxPC(PlayerOwner).bExitingServer)
    {
        return;
    }
    GRI = FoxGRI(WorldInfo.GRI);
    GRI.FirstPlaceID = inFirstPlaceID;
    GRI.SecondPlaceID = inSecondPlaceID;
    GRI.ThirdPlaceID = inThirdPlaceID;
    GRI.ForthPlaceID = inForthPlaceID;
    WinningTeamIndex = InWinningTeamIndex;
    MatchEndXP = GameEndExperience;
    TransitionToEndGameResults();
    //return;    
}

function ShowEndGameResults(float XPos, float YPos)
{
    local FoxGRI_OS GRI;
    local FoxPRI MyPRI;
    local bool bDidWin;

    GRI = FoxGRI_OS(WorldInfo.GRI);
    MyPRI = FoxPRI(PCOwner.PlayerReplicationInfo);
    SetupHeroPose();
    bDidWin = (((MyPRI != none) && MyPRI.Team != none) ? MyPRI.Team.TeamIndex == WinningTeamIndex : false);
    // End:0x15F
    if(bDidWin)
    {
        DynamicUI.as_showMatchResults(XPos, YPos, "DM", LocStringMissionSucceeded, bDidWin, "", "", 0, 0, 0, LocStringSubMatchOver, 4.0);
    }
    // End:0x2DA
    else
    {
        // End:0x27A
        if(GRI.RemainingTime > 0)
        {
            // End:0x1F5
            if(!AreAnyPlayersAlive())
            {
                DynamicUI.as_showMatchResults(XPos, YPos, "DM", LocStringMissionFailed, bDidWin, "", "", 0, 0, 0, LocStringMissionFailedDesc, 4.0);
            }
            // End:0x277
            else
            {
                // End:0x277
                if(GRI.bPointActive)
                {
                    DynamicUI.as_showMatchResults(XPos, YPos, "DM", LocStringMissionFailed, bDidWin, "", "", 0, 0, 0, LocStringObjectiveFailed, 4.0);
                }
            }
        }
        // End:0x2DA
        else
        {
            DynamicUI.as_showMatchResults(XPos, YPos, "DM", LocStringMissionFailed, bDidWin, "", "", 0, 0, 0, LocStringMIssionFailedTime, 4.0);
        }
    }
    //return;    
}

function GenerateMVPWidgets()
{
    local FoxPRI FirstPRI, SecondPRI, ThirdPRI, ForthPRI;
    local FoxGRI GRI;
    local EmblemPanelEntry Emblem;
    local array<string> DetailDataProvider;

    GRI = FoxGRI(WorldInfo.GRI);
    FirstPRI = GRI.GetPRIByPlayerID(GRI.FirstPlaceID);
    SecondPRI = GRI.GetPRIByPlayerID(GRI.SecondPlaceID);
    ThirdPRI = GRI.GetPRIByPlayerID(GRI.ThirdPlaceID);
    ForthPRI = GRI.GetPRIByPlayerID(GRI.ForthPlaceID);
    // End:0x1C3
    if(FirstPRI != none)
    {
        DetailDataProvider = GetMVPDetail(FirstPRI, LocStringFirstPlace, Emblem);
        DynamicUI.as_addMVP(-410, 190, DetailDataProvider, Emblem);
    }
    // End:0x236
    if(SecondPRI != none)
    {
        DetailDataProvider = GetMVPDetail(SecondPRI, LocStringSecondPlace, Emblem);
        DynamicUI.as_addMVP(60, 190, DetailDataProvider, Emblem);
    }
    // End:0x2AF
    if(ThirdPRI != none)
    {
        DetailDataProvider = GetMVPDetail(ThirdPRI, LocStringThirdPlace, Emblem);
        DynamicUI.as_addMVP(-600, 270, DetailDataProvider, Emblem);
    }
    // End:0x325
    if(ForthPRI != none)
    {
        DetailDataProvider = GetMVPDetail(ForthPRI, LocStringFourthPlace, Emblem);
        DynamicUI.as_addMVP(240, 270, DetailDataProvider, Emblem);
    }
    //return;    
}

function bool ShouldShowHeroPose()
{
    return WinningTeamIndex == 0;
    //return ReturnValue;    
}

function NotifyObjectiveActive()
{
    AddNewGameplayUpdate(LocStringNewObjective);
    //return;    
}

function NotifyObjectiveComplete()
{
    AddNewGameplayUpdate(LocStringObjectiveComplete);
    //return;    
}

function OnShowTieBreaker()
{
    //return;    
}

defaultproperties
{
    LocStringFirstWave="BREACH DETECTED...ETA 15 SECONDS"
    LocStringFinalWave="FINAL BREACH DETECTED"
    LocStringNextWave="NEW BREACH DETECTED"
    LocStringWaveComplete="THREAT ELIMINATED"
    LocStringFinalWaveComplete="NO THREAT DETECTED...PERIMETER SECURE"
    LocStringAllPlayersDead="ALL PLAYERS KILLED"
    LocStringMissionFailed="MISSION FAILED"
    LocStringMissionSucceeded="MISSION SUCCEEDED"
    LocStringMissionFailedDesc="ALL TEAM MEMBERS ARE DEAD"
    LocStringMIssionFailedTime="BREACH NOT CONTAINED IN TIME"
    LocStringTeammateDown="IS DOWN!"
    LocStringDepotChanged="DEPOT UPLINK LOST..."
    LocStringDepotReboot="ESTABLISHING NEW DEPOT UPLINK"
    LocStringNewObjective="NEW OBJECTIVE POINT ACTIVATED"
    LocStringObjectiveComplete="OBJECTIVE COMPLETED"
    LocStringObjectiveFailed="YOU FAILED TO CAPTURE THE OBJECTIVE"
    LocStringSubMatchOver="YOU HAVE SURVIVED THE ONSLAUGHT!"
    bUseBleedOutReviveTarget=true
    GameClass=class'FoxGameMP_OS'
    HudUIClass=class'FoxHudUI_OS'
    LocStringRoundInProgress="You are dead."
    LocStringWaitingForRoundEnd="Waiting for wave to end..."
    LocStringWaitingForRoundStart="Waiting for next wave to start..."
    LocStringWaitingForGameEnd="Waiting for wave to end..."
}