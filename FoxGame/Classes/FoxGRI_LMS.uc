/*******************************************************************************
 * FoxGRI_LMS generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxGRI_LMS extends FoxGRI_DM
    config(Game)
    hidecategories(Navigation,Movement,Collision);

defaultproperties
{
    MaxPersonalSpawnTickets=1
    ScoringInfoClass=class'FoxScoringInfoLMS'
    GameTypeCode="LMS"
    GameClass=class'FoxGameMP_LMS'
}