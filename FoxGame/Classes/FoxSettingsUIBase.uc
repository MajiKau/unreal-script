/*******************************************************************************
 * FoxSettingsUIBase generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxSettingsUIBase extends Object within FoxMenuUI
    abstract
    native(UI)
    config(UI);

var transient class<FoxSettingsUIBase> NextSettingsClass;
var transient FoxProfileSettings ProfileSettings;
var const config string SettingsLocalizationSectionName;
var int NumSettings;
var array<float> OriginalVals;
var array<float> CurrentVals;
var array<float> DefaultVals;
var array<FoxSettingsWidget> SettingsWidgets;
var array<string> EnabledDisabledStrings;
var array<string> ActiveInactiveStrings;
var array<string> VisibleHiddenStrings;
var array<string> NumberStepperOfThroughMax;
var const config ButtonContainerWidget SettingsContainerProperties;
var const config ButtonContainerWidget SettingsDividerContainerProperties;

function Initialize()
{
    ProfileSettings = Outer.Outer.PCOwner.ProfileSettings;
    SetupStrings();
    SetupSettingsScene();
    NumSettings = SettingsWidgets.Length;
    OriginalVals.Length = NumSettings;
    CurrentVals.Length = NumSettings;
    DefaultVals.Length = NumSettings;
    GetOriginalVals();
    GetDefaultVals();
    Outer.Outer.Advance(0.0);
    ei_RevertChanges();
    SetupButtonContainer();
    ConfirmSettingsValidity();
    //return;    
}

function SetupStrings()
{
    EnabledDisabledStrings.AddItem(Outer.LocalizeMenuString("Disabled"));
    EnabledDisabledStrings.AddItem(Outer.LocalizeMenuString("Enabled"));
    ActiveInactiveStrings.AddItem(Outer.LocalizeMenuString("Inactive"));
    ActiveInactiveStrings.AddItem(Outer.LocalizeMenuString("Active"));
    VisibleHiddenStrings.AddItem(Outer.LocalizeMenuString("Hidden"));
    VisibleHiddenStrings.AddItem(Outer.LocalizeMenuString("Visible"));
    NumberStepperOfThroughMax.AddItem(("00 | 10 [" $ Outer.LocalizeMenuString("Off")) $ "]");
    NumberStepperOfThroughMax.AddItem(("01 | 10 [" $ Outer.LocalizeMenuString("Minimum")) $ "]");
    NumberStepperOfThroughMax.AddItem(("02 | 10 [" $ Outer.LocalizeMenuString("Minimum")) $ "]");
    NumberStepperOfThroughMax.AddItem(("03 | 10 [" $ Outer.LocalizeMenuString("Low")) $ "]");
    NumberStepperOfThroughMax.AddItem(("04 | 10 [" $ Outer.LocalizeMenuString("Low")) $ "]");
    NumberStepperOfThroughMax.AddItem(("05 | 10 [" $ Outer.LocalizeMenuString("Medium")) $ "]");
    NumberStepperOfThroughMax.AddItem(("06 | 10 [" $ Outer.LocalizeMenuString("Medium")) $ "]");
    NumberStepperOfThroughMax.AddItem(("07 | 10 [" $ Outer.LocalizeMenuString("High")) $ "]");
    NumberStepperOfThroughMax.AddItem(("08 | 10 [" $ Outer.LocalizeMenuString("High")) $ "]");
    NumberStepperOfThroughMax.AddItem(("09 | 10 [" $ Outer.LocalizeMenuString("Maximum")) $ "]");
    NumberStepperOfThroughMax.AddItem(("10 | 10 [" $ Outer.LocalizeMenuString("Maximum")) $ "]");
    //return;    
}

function SetupSettingsScene()
{
    //return;    
}

protected function SetupButtonContainer()
{
    Outer.as_addAdditionalButtonContainer("container", 1000, 290, 0, 0, 0, 100, "", "", 15, 1200, 1200, false);
    // End:0x186
    if(class'WorldInfo'.static.IsConsoleBuild())
    {
        as_addSettingsButtonPrompt(Outer.LocalizeMenuString("RevertChanges_X"), Outer.LocalizeMenuString("RevertChangesToolTip"), false, "PromptRevertChanges");
        as_addSettingsButtonPrompt(Outer.LocalizeMenuString("ResetToDefaults_X"), Outer.LocalizeMenuString("ResetToDefaultsToolTip"), false, "PromptResetToDefaults");
    }
    // End:0x318
    else
    {
        as_addSettingsButtonPrompt(Outer.LocalizeMenuString("ApplyChanges"), Outer.LocalizeMenuString("ApplyChangesToolTip"), false, "ei_ApplyChanges");
        as_addSettingsButtonPrompt(Outer.LocalizeMenuString("RevertChanges"), Outer.LocalizeMenuString("RevertChangesToolTip"), false, "PromptRevertChanges");
        as_addSettingsButtonPrompt(Outer.LocalizeMenuString("ResetToDefaults"), Outer.LocalizeMenuString("ResetToDefaultsToolTip"), false, "PromptResetToDefaults");
    }
    //return;    
}

function GetOriginalVals()
{
    //return;    
}

function GetDefaultVals()
{
    //return;    
}

function TryClose(class<FoxSettingsUIBase> TheNextSettingsClass)
{
    NextSettingsClass = TheNextSettingsClass;
    BeginClose();
    //return;    
}

function BeginClose(optional bool bSkipTransition)
{
    bSkipTransition = false;
    ei_ApplyChanges();
    OnSettingsClose();
    // End:0x70
    if(NextSettingsClass != none)
    {
        Outer.Outer.PendingCommandGroupName = "optionsNextPage";
    }
    // End:0x90
    else
    {
        Outer.Settings = none;
    }
    // End:0xF1
    if(!bSkipTransition)
    {
        Outer.Outer.as_transition(Outer.SettingsOutTransition);
    }
    //return;    
}

function OnSettingsClose()
{
    //return;    
}

function ei_SettingChanged(string WidgetIndexString, string NewSelectedIndexString)
{
    CurrentVals[int(WidgetIndexString)] = float(int(NewSelectedIndexString));
    //return;    
}

function ApplyChanges()
{
    //return;    
}

function DialogRevertChanges(FoxDialogBoxBase.EDialogBoxConfirmType Selection)
{
    // End:0x1F
    if(Selection == 0)
    {
        ei_RevertChanges();
    }
    //return;    
}

event PromptRevertChanges()
{
    local DialogBoxProperties Properties;

    // End:0x61
    if((Outer.Outer.DialogBox != none) || Outer.SocialTab != none)
    {
        return;
    }
    Properties.DialogType = 2;
    Properties.TitleText = Outer.LocalizeMenuString("RevertChangesDialogTitle", SettingsLocalizationSectionName);
    Properties.BodyText = Outer.LocalizeMenuString("RevertChangesMessage", SettingsLocalizationSectionName);
    Properties.OnDialogButtonPressed = DialogRevertChanges;
    Outer.Outer.ShowDialogBox(Properties);
    //return;    
}

function ei_RevertChanges(optional bool bSkipUpdate)
{
    local int I, SettingIndex;

    bSkipUpdate = false;
    I = 0;
    J0x10:
    // End:0x5C [Loop If]
    if(I < NumSettings)
    {
        CurrentVals[I] = OriginalVals[I];
        ++ I;
        // [Loop Continue]
        goto J0x10;
    }
    // End:0x123
    if(!bSkipUpdate)
    {
        I = 0;
        J0x76:
        // End:0x123 [Loop If]
        if(I < SettingsWidgets.Length)
        {
            SettingIndex = int(SettingsWidgets[I].GetString("extClickInterfaceArg"));
            SettingsWidgets[I].as_SetValue(OriginalVals[SettingIndex]);
            ++ I;
            // [Loop Continue]
            goto J0x76;
        }
    }
    //return;    
}

function DialogResetToDefaults(FoxDialogBoxBase.EDialogBoxConfirmType Selection)
{
    // End:0x1E
    if(Selection == 0)
    {
        ei_ResetToDefaults();
    }
    //return;    
}

event PromptResetToDefaults()
{
    local DialogBoxProperties Properties;

    // End:0x61
    if((Outer.Outer.DialogBox != none) || Outer.SocialTab != none)
    {
        return;
    }
    Properties.DialogType = 2;
    Properties.TitleText = Outer.LocalizeMenuString("ResetToDefaultsDialogTitle", SettingsLocalizationSectionName);
    Properties.BodyText = Outer.LocalizeMenuString("ResetToDefaultsMessage", SettingsLocalizationSectionName);
    Properties.OnDialogButtonPressed = DialogResetToDefaults;
    Outer.Outer.ShowDialogBox(Properties);
    //return;    
}

function ei_ResetToDefaults()
{
    local int I, SettingIndex;

    I = 0;
    J0x0B:
    // End:0x109 [Loop If]
    if(I < NumSettings)
    {
        SettingIndex = int(SettingsWidgets[I].GetString("extClickInterfaceArg"));
        // End:0xFB
        if(CurrentVals[SettingIndex] != DefaultVals[SettingIndex])
        {
            CurrentVals[SettingIndex] = DefaultVals[SettingIndex];
            SettingsWidgets[I].as_SetValue(DefaultVals[SettingIndex]);
        }
        ++ I;
        // [Loop Continue]
        goto J0x0B;
    }
    //return;    
}

function SaveChanges()
{
    local int I;

    I = 0;
    J0x0B:
    // End:0x82 [Loop If]
    if(I < NumSettings)
    {
        // End:0x74
        if(OriginalVals[I] != CurrentVals[I])
        {
            OriginalVals[I] = CurrentVals[I];
        }
        ++ I;
        // [Loop Continue]
        goto J0x0B;
    }
    //return;    
}

function ei_ApplyChanges()
{
    // End:0x42
    if(HasSettingsChanged())
    {
        SaveChanges();
        ApplyChanges();
        Outer.bRequiresProfileSave = true;
    }
    //return;    
}

protected function AddOptionToggle(string Label, string ToolTip, optional string ExtInterfaceArg, optional int SelectedIndex)
{
    local string ObjectPath;

    ObjectPath = Outer.as_addToggleButton(Outer.LocalizeMenuString(Label), Outer.LocalizeMenuString(ToolTip), EnabledDisabledStrings, SelectedIndex, "ei_settingChanged", ExtInterfaceArg);
    SettingsWidgets.AddItem(FoxSettingsWidget(Outer.Outer.GetVariableObject(ObjectPath, class'FoxSettingsWidget')));
    //return;    
}

protected function AddOptionStepper(string Label, string ToolTip, const out array<string> Data, optional string ExtInterfaceArg, optional int SelectedIndex, optional bool Loop)
{
    local string ObjectPath;

    ObjectPath = Outer.as_addOptionStepper(Outer.LocalizeMenuString(Label), Outer.LocalizeMenuString(ToolTip), Data, SelectedIndex, "ei_settingChanged", ExtInterfaceArg, Loop);
    SettingsWidgets.AddItem(FoxSettingsWidget(Outer.Outer.GetVariableObject(ObjectPath, class'FoxSettingsWidget')));
    //return;    
}

function bool HasSettingsChanged()
{
    local int I;

    I = 0;
    J0x0B:
    // End:0x5D [Loop If]
    if(I < NumSettings)
    {
        // End:0x4F
        if(CurrentVals[I] != OriginalVals[I])
        {
            return true;
        }
        ++ I;
        // [Loop Continue]
        goto J0x0B;
    }
    return false;
    //return ReturnValue;    
}

function ConfirmSettingsValidity()
{
    //return;    
}

function as_addSettingsButton(string Label, string ToolTip, bool bIsLoginButton, string extClickInterfaceFunc, optional string extClickInterfaceArg)
{
    Outer.Outer.ActionScriptVoid("_root.addSettingsButton");
    //return;    
}

function as_addSettingsButtonPrompt(string Label, string ToolTip, bool bIsLoginButton, string extClickInterfaceFunc, optional string extClickInterfaceArg)
{
    Outer.Outer.ActionScriptVoid("_root.addSettingsButtonPrompt");
    //return;    
}

function string as_addSettingsDropDownMenu(string Label, string ToolTip, array<string> Data, string extClickInterfaceFunc, int InitializeIndex)
{
    return Outer.Outer.ActionScriptString("_root.addSettingsDropDownMenu");
    //return ReturnValue;    
}

function as_placeHorizLinesSettings(string instancename, int PosX, int PosY, string RemoteVersion)
{
    Outer.Outer.ActionScriptVoid("_root.placeHorizLinesSettings");
    //return;    
}

function as_addSettingsSpacer()
{
    Outer.Outer.ActionScriptVoid("_root.addSettingsSpacer");
    //return;    
}

function as_addSettingsBG(int PosX, int PosY)
{
    Outer.Outer.ActionScriptVoid("_root.addSettingsBG");
    //return;    
}

function as_addSettingsKeybindBG(int PosX, int PosY)
{
    // End:0x7B
    if(!class'WorldInfo'.static.IsConsoleBuild())
    {
        Outer.Outer.ActionScriptVoid("_root.addSettingsKeybindBG_PC");
    }
    // End:0xCB
    else
    {
        Outer.Outer.ActionScriptVoid("_root.addSettingsKeybindBG");
    }
    //return;    
}

defaultproperties
{
    SettingsLocalizationSectionName="Settings"
    SettingsContainerProperties=(X=1000.0,Y=-218.0,Z=0.0,FOV=0.0,YRot=0.0,Opacity=0.0,WidgetSpacing=-16,MaxListWidth=1200,MaxListHeight=1200,bVerticalAlign=false)
    SettingsDividerContainerProperties=(X=1000.0,Y=-204.0,Z=0.0,FOV=0.0,YRot=0.0,Opacity=0.0,WidgetSpacing=2,MaxListWidth=500,MaxListHeight=800,bVerticalAlign=false)
}