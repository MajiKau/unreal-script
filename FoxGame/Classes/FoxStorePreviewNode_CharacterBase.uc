/*******************************************************************************
 * FoxStorePreviewNode_CharacterBase generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxStorePreviewNode_CharacterBase extends FoxStorePreviewNode_PreviewBase within FoxStoreUIItemDetail
    native(UI)
    config(UI);

var FoxArmoryUI.EArmorItem CustomItemType;
var FoxPawnModInfo PawnModInfo;

function ApplyItemStats(const out ProfileGearInfo GearInfo)
{
    local array<StatPanelEntry> StatData;
    local array<UIResourceDataProvider> ProviderArray;
    local int I;
    local FoxDataProvider_GearInfo GearProvider;

    PawnModInfo = default.PawnModInfo;
    // End:0x470
    if(Outer.Outer.Outer.Outer.UnlockablesDataStore != none)
    {
        // End:0x159
        if(Outer.PreviewNodeType == 21)
        {
            GearProvider = Outer.Outer.Outer.Outer.UnlockablesDataStore.GetGearProviderFromUnlockID(ItemUnlockID);
            class'FoxStatsUI'.static.GetFormattedStatsFromGearProvider(StatData, GearProvider);
        }
        // End:0x470
        else
        {
            switch(Outer.PreviewNodeType)
            {
                // End:0x205
                case 25:
                    ProviderArray = Outer.Outer.Outer.Outer.UnlockablesDataStore.HelmetProviderArray;
                    // End:0x394
                    break;
                // End:0x289
                case 24:
                    ProviderArray = Outer.Outer.Outer.Outer.UnlockablesDataStore.UpperBodyProviderArray;
                    // End:0x394
                    break;
                // End:0x30D
                case 23:
                    ProviderArray = Outer.Outer.Outer.Outer.UnlockablesDataStore.LowerBodyProviderArray;
                    // End:0x394
                    break;
                // End:0x391
                case 33:
                    ProviderArray = Outer.Outer.Outer.Outer.UnlockablesDataStore.BadgeProviderArray;
                    // End:0x394
                    break;
                // End:0xFFFF
                default:
                    I = 0;
                    J0x39F:
                    // End:0x470 [Loop If]
                    if(I < ProviderArray.Length)
                    {
                        // End:0x462
                        if(FoxDataProvider_GearInfo(ProviderArray[I]).UnlockID == ItemUnlockID)
                        {
                            PawnModInfo = FoxDataProvider_GearInfo(ProviderArray[I]).PawnMods;
                            class'FoxStatsUI'.static.GetFormattedStatsFromPawnModifiers(StatData, PawnModInfo);
                        }
                        ++ I;
                        // [Loop Continue]
                        goto J0x39F;
                    }
                }
            }
            // End:0x4A8
            if(StatData.Length > 0)
            {
                Outer.as_addStoreDetailStats(StatData);
            }
            //return;            
}

function MakePreviewActor()
{
    // End:0x270
    if(FoxCustomPlayerActor(Outer.PreviewActor) == none)
    {
        // End:0x7E
        if(Outer.PreviewActor != none)
        {
            Outer.PreviewActor.Destroy();
        }
        Outer.PreviewActor = Outer.Outer.Outer.Outer.PCOwner.Spawn(class'FoxCustomPlayerActor', Outer.Outer.Outer.Outer.PCOwner,, Outer.PreviewSpawnPoint.Location, Outer.PreviewSpawnPoint.Rotation);
        Outer.PreviewActor.OwnerPC = Outer.Outer.Outer.Outer.PCOwner;
    }
    Outer.PreviewActor.CustomItemType = GetCustomItemType();
    Outer.PreviewActor.HighlightItemType = GetCustomItemType();
    UpdatePreviewActor();
    FoxCustomPlayerActor(Outer.PreviewActor).IdleAnimName = 'Armory_Idle_A';
    FoxCustomPlayerActor(Outer.PreviewActor).StartIdleAnimation(0.0);
    Outer.PreviewActor.MoveWeaponToIdealPosition(float(WeaponXPos), float(WeaponYPos), float(WeaponWidth), float(WeaponHeight));
    Outer.Outer.Outer.Outer.PCOwner.SetViewTarget(Outer.PreviewActor);
    Outer.Outer.Outer.Outer.PCOwner.PlayerCamera.DoUpdateCamera(0.0);
    //return;    
}

function UpdatePreviewActor()
{
    local ProfileGearInfo GearInfo;

    SetupGear(GearInfo);
    FoxCustomPlayerActor(Outer.PreviewActor).UpdateGearInfo(GearInfo);
    ApplyItemStats(GearInfo);
    //return;    
}

function SetupGear(out ProfileGearInfo GearInfo)
{
    local int NewIndex;
    local FoxLoadoutInfo MyLoadout;

    MyLoadout = FoxPRI(Outer.Outer.Outer.Outer.PCOwner.PlayerReplicationInfo).Loadout;
    GearInfo = MyLoadout.GetCurrentGearInfo();
    NewIndex = FoxPRI(Outer.Outer.Outer.Outer.PCOwner.PlayerReplicationInfo).Loadout.GetGearIndexById(ItemUnlockID, Outer.PreviewNodeType);
    switch(Outer.PreviewNodeType)
    {
        // End:0x1F2
        case 25:
            GearInfo.HelmetID = byte(NewIndex);
            // End:0x2B7
            break;
        // End:0x222
        case 23:
            GearInfo.LowerBodyID = byte(NewIndex);
            // End:0x2B7
            break;
        // End:0x252
        case 24:
            GearInfo.UpperBodyID = byte(NewIndex);
            // End:0x2B7
            break;
        // End:0x284
        case 33:
            GearInfo.BadgeID = byte(NewIndex);
            // End:0x2B7
            break;
        // End:0x2B4
        case 21:
            GearInfo.TacticalID = byte(NewIndex);
            // End:0x2B7
            break;
        // End:0xFFFF
        default:
            //return;
    }    
}

function FoxArmoryUI.EArmorItem GetCustomItemType()
{
    switch(Outer.PreviewNodeType)
    {
        // End:0x30
        case 8:
            return 4;
        // End:0x38
        case 25:
            return 0;
        // End:0x40
        case 24:
            return 1;
        // End:0x48
        case 23:
            return 2;
        // End:0x50
        case 33:
            return 3;
        // End:0x58
        case 21:
            return 9;
        // End:0xFFFF
        default:
            return 4;
    }
    //return ReturnValue;    
}

defaultproperties
{
    PawnModInfo=(SwitchWeaponSpeed=0,MeleeRange=0,Health=0,HealthRecharge=0,MovementSpeed=0,Stamina=0,HRVDuration=0,HRVRechargeRate=0,GearSlots=0,bCalculated=false,HelmetDamageReduction=0,BodyDamageReduction=0,LegsDamageReduction=0,SprintMultiplier=1.0,ToxicProtection=0,PermanentHealthProtection=0,IncendiaryProtection=0,ExplosiveProtection=0,ElectroProtection=0,MeleeProtection=0,InfraredProtection=0)
}