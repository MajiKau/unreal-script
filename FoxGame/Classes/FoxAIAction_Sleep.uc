/*******************************************************************************
 * FoxAIAction_Sleep generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxAIAction_Sleep extends FoxAIAction
    native(AI)
    editinlinenew
    hidecategories(Object);

var() string AbortTriggerString;
var() string FinishedTriggerString;
var() bool bRandomizeDuration;
var() float Durationmax;

// Export UFoxAIAction_Sleep::execInitialize(FFrame&, void* const)
native function bool Initialize();

// Export UFoxAIAction_Sleep::execProcessBehaviorEvent(FFrame&, void* const)
native function bool ProcessBehaviorEvent(string EventName, optional Actor EventInstigator, optional Actor EventRecipient, optional Vector EventLocation);

defaultproperties
{
    AbortTriggerString="Abort"
    FinishedTriggerString="Finished"
    Duration=1.0
    StateString="Sleep"
}