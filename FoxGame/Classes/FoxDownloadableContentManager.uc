/*******************************************************************************
 * FoxDownloadableContentManager generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxDownloadableContentManager extends DownloadableContentManager;

var transient bool bHasRefreshed;

event Init()
{
    bHasRefreshed = false;
    super.Init();
    __OnRefreshComplete__Delegate = RefreshDLCComplete;
    //return;    
}

function RefreshDLC()
{
    // End:0x19
    if(!bHasRefreshed)
    {
        super.RefreshDLC();
    }
    //return;    
}

function RefreshDLCComplete()
{
    local DataStoreClient DSClient;
    local FoxDataStore_Unlockables UnlockablesDataStore;

    bHasRefreshed = true;
    DSClient = class'UIInteraction'.static.GetDataStoreClient();
    // End:0xAE
    if(DSClient != none)
    {
        UnlockablesDataStore = FoxDataStore_Unlockables(DSClient.FindDataStore('FoxUnlockables'));
        // End:0xAE
        if(UnlockablesDataStore != none)
        {
            UnlockablesDataStore.GenerateCachedUnlocks();
        }
    }
    //return;    
}
