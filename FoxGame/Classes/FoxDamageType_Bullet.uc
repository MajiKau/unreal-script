/*******************************************************************************
 * FoxDamageType_Bullet generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxDamageType_Bullet extends FoxDamageType;

defaultproperties
{
    bAllowDecapitation=true
    DamageZoneMultipliers(0)=1.0
    DamageZoneMultipliers(1)=2.0
    DamageZoneMultipliers(2)=1.0
    DamageZoneMultipliers(3)=1.0
    DamageZoneMultipliers(4)=1.0
    DamageZoneMultipliers(5)=1.0
    DamageZoneMultipliers(6)=1.0
    DamageZoneMultipliers(7)=1.0
    DamageZoneMultipliers(8)=1.0
    DamageZoneMultipliers(9)=1.0
    ProtectionType=EDamageReductionType.EDRT_NormalAmmoDamageReduction
    BodyHitType=EHitType.EHT_BodyHit
    bCausesFracture=true
    bSuppressDeathMessage=false
    bAwardNoPoints=false
    KDamageImpulse=1000.0
    DamagedFFWaveform=ForceFeedbackWaveform'Default__FoxDamageType_Bullet.ForceFeedbackWaveformDamage'
    KilledFFWaveform=ForceFeedbackWaveform'Default__FoxDamageType_Bullet.ForceFeedbackWaveformDeath'
}