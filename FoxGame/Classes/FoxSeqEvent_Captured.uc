/*******************************************************************************
 * FoxSeqEvent_Captured generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxSeqEvent_Captured extends SequenceEvent
    forcescriptorder(true)
    hidecategories(Object);

defaultproperties
{
    bPlayerOnly=false
    ObjName="Captured"
}