/*******************************************************************************
 * FoxStorePreviewNode_Taunts generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxStorePreviewNode_Taunts extends FoxStorePreviewNode_CharacterBase within FoxStoreUIItemDetail
    native(UI)
    config(UI);

var FoxDataProvider_Emote CurrentEmote;

function MakePreviewActor()
{
    super.MakePreviewActor();
    CurrentEmote = FoxDataStore_Unlockables(class'UIInteraction'.static.GetDataStoreClient().FindDataStore('FoxUnlockables')).GetEmoteFromUnlockID(ItemUnlockID);
    // End:0x136
    if(CurrentEmote != none)
    {
        FoxCustomPlayerActor(Outer.PreviewActor).StartFullBodyAnimation(CurrentEmote.ThirdPersonAnimName, 0.250, true);
        FoxCustomPlayerActor(Outer.PreviewActor).StartMeshEffects(CurrentEmote);
    }
    //return;    
}

function FoxArmoryUI.EArmorItem GetCustomItemType()
{
    return 4;
    //return ReturnValue;    
}
