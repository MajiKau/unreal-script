/*******************************************************************************
 * FoxCustomizationNode_CamoBody generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxCustomizationNode_CamoBody extends FoxCustomizationNode_CamoBase within FoxArmoryUI
    config(UI);

function Initialize(optional bool bSelectEquipped)
{
    local ScrollerListEntry NewScrollerListEntry;
    local CamoInfo NewCamoInfo;
    local int I;

    bSelectEquipped = true;
    super(FoxCustomizationNode_Base).Initialize(bSelectEquipped);
    // End:0x66
    if(bSelectEquipped)
    {
        Outer.CustomItemType = 4;
        Outer.ResetSelectedLoadout();
    }
    CamoInfoArray.Length = 0;
    ScrollerEntries.Length = 0;
    Outer.SelectedScrollerIndex = 0;
    EquippedUnlockID = ((Outer.SavedGearSlot.BodyCamoID != 255) ? class'FoxUnlockInfo'.default.BodyCamoUnlocks[Outer.SavedGearSlot.BodyCamoID].UnlockID : 0);
    I = 0;
    J0x157:
    // End:0x267 [Loop If]
    if(I < class'FoxUnlockInfo'.default.BodyCamoUnlocks.Length)
    {
        NewCamoInfo.CamoIndex = I;
        // End:0x259
        if(SetScrollerEntry(NewScrollerListEntry, class'FoxUnlockInfo'.default.BodyCamoUnlocks[I].UnlockID, bSelectEquipped))
        {
            NewScrollerListEntry.InsertIndex = CamoInfoArray.Length;
            ScrollerEntries.AddItem(NewScrollerListEntry);
            CamoInfoArray.AddItem(NewCamoInfo);
        }
        ++ I;
        // [Loop Continue]
        goto J0x157;
    }
    SetupScrollerBar(bSelectEquipped);
    //return;    
}

function ei_changeScrollerIndex(int Index)
{
    local ProfileGearInfo TempGearInfo;

    // End:0xFD
    if((Index >= 0) && Index < ScrollerEntries.Length)
    {
        TempGearInfo = Outer.CachedGearSlot;
        TempGearInfo.BodyCamoID = byte(CamoInfoArray[ScrollerEntries[Index].InsertIndex].CamoIndex);
        TempGearInfo.AvatarID = -1;
        Outer.UpdateCharacterGear(TempGearInfo);
    }
    super(FoxCustomizationNode_Base).ei_changeScrollerIndex(Index);
    //return;    
}

function ei_ScrollerClicked(string SelectedIndex)
{
    local int NextIndex;

    NextIndex = int(SelectedIndex);
    // End:0x86
    if((NextIndex >= 0) && NextIndex < ScrollerEntries.Length)
    {
        Outer.SelectedScrollerUnlock = ScrollerEntries[NextIndex].UnlockID;
    }
    // End:0xA6
    else
    {
        Outer.SelectedScrollerUnlock = 0;
    }
    super(FoxCustomizationNode_Base).ei_ScrollerClicked(SelectedIndex);
    //return;    
}

function bool IsPurchased(int ItemId)
{
    return FoxServerConnection(Outer.Outer.Outer.PCOwner.WorldInfo.ServerConn).IsBodyCamoPurchased(ItemId, FoxPRI(Outer.Outer.Outer.PCOwner.PlayerReplicationInfo));
    //return ReturnValue;    
}

function NodeEquipButtonClicked(string SelectedIndex)
{
    local int PrevGearID, NextIndex;

    Outer.SavedAvatarIndex = -1;
    Outer.LastSelectedAvatarIndex = Outer.SavedAvatarIndex;
    Outer.CachedAvatarIndex = Outer.SavedAvatarIndex;
    NextIndex = int(SelectedIndex);
    // End:0x269
    if((NextIndex >= 0) && NextIndex < ScrollerEntries.Length)
    {
        PrevGearID = Outer.CachedGearSlot.BodyCamoID;
        Outer.CachedGearSlot.BodyCamoID = byte(CamoInfoArray[ScrollerEntries[NextIndex].InsertIndex].CamoIndex);
        Outer.CachedAvatarIndex = -1;
        Outer.CachedGearSlot.AvatarID = -1;
        // End:0x269
        if(PrevGearID != Outer.CachedGearSlot.BodyCamoID)
        {
            Outer.UpdateCharacterGear(Outer.CachedGearSlot);
        }
    }
    super(FoxCustomizationNode_Base).NodeEquipButtonClicked(SelectedIndex);
    Outer.Outer.ArmoryItemGrid.as_SetSelectedModIndex(Outer.SetModDataForCamo(Outer.CustomItemType), true);
    //return;    
}

defaultproperties
{
    bComponentSlots=true
}