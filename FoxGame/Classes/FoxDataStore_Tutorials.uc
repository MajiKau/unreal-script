/*******************************************************************************
 * FoxDataStore_Tutorials generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxDataStore_Tutorials extends UIDataStore_GameResource
    transient
    native
    config(Game)
    hidecategories(Object,UIRoot);

var private array<FoxDataProvider_Tutorial> TutorialProviders;

event Registered(LocalPlayer PlayerOwner)
{
    local int I;
    local array<UIResourceDataProvider> Providers;

    GetAllResourceDataProviders(class'FoxDataProvider_Tutorial', Providers);
    TutorialProviders.Length = 0;
    I = 0;
    J0x33:
    // End:0xA4 [Loop If]
    if(I < Providers.Length)
    {
        // End:0x96
        if(FoxDataProvider_Tutorial(Providers[I]) != none)
        {
            TutorialProviders.AddItem(FoxDataProvider_Tutorial(Providers[I]));
        }
        ++ I;
        // [Loop Continue]
        goto J0x33;
    }
    super(UIDataStore).Registered(PlayerOwner);
    //return;    
}

event Unregistered(LocalPlayer PlayerOwner)
{
    TutorialProviders.Length = 0;
    super(UIDataStore).Unregistered(PlayerOwner);
    //return;    
}

function bool GetTutorialProviderByName(const name TutorialName, out FoxDataProvider_Tutorial TutorialProvider, optional out int TutorialIndex)
{
    local int Index;

    Index = GetTutorialIndexByName(TutorialName);
    // End:0x63
    if(Index != -1)
    {
        TutorialIndex = Index;
        TutorialProvider = TutorialProviders[TutorialIndex];
        return true;
    }
    return false;
    //return ReturnValue;    
}

function int GetTutorialIndexByName(const name TutorialName)
{
    local int I;

    I = 0;
    J0x0B:
    // End:0x71 [Loop If]
    if(I < TutorialProviders.Length)
    {
        // End:0x63
        if(TutorialProviders[I].Name == TutorialName)
        {
            return I;
        }
        ++ I;
        // [Loop Continue]
        goto J0x0B;
    }
    return -1;
    //return ReturnValue;    
}

defaultproperties
{
    Tag=FoxTutorials
}