/*******************************************************************************
 * FoxStorePreviewNode_Avatar generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxStorePreviewNode_Avatar extends FoxStorePreviewNode_CharacterBase within FoxStoreUIItemDetail
    config(UI);

function SetupGear(out ProfileGearInfo GearInfo)
{
    //return;    
}

function FoxArmoryUI.EArmorItem GetCustomItemType()
{
    return 4;
    //return ReturnValue;    
}
