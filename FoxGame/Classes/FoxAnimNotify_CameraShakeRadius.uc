/*******************************************************************************
 * FoxAnimNotify_CameraShakeRadius generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxAnimNotify_CameraShakeRadius extends AnimNotify_Scripted
    editinlinenew
    collapsecategories
    hidecategories(Object);

var() export editinline CameraShake Shake;
var() float MaxRadius;
var() bool bScaleShakeByRadius;

event Notify(Actor Owner, AnimNodeSequence AnimSeqInstigator)
{
    local FoxPawn OtherP;
    local float Strength;

    // End:0x186
    foreach Owner.WorldInfo.AllPawns(class'FoxPawn', OtherP, Owner.Location, MaxRadius)
    {
        // End:0x185
        if(OtherP.IsAliveAndWell() && OtherP.IsHumanControlled())
        {
            Strength = ((bScaleShakeByRadius) ? 1.0 - FClamp(VSize(OtherP.Location - Owner.Location) / MaxRadius, 0.0, 1.0) : 1.0);
            PlayerController(OtherP.Controller).ClientPlayCameraShake(Shake, Strength);
        }        
    }    
    //return;    
}

defaultproperties
{
    begin object name=Shake0 class=CameraShake
        bSingleInstance=true
        OscillationDuration=1.0
        RotOscillation=(Pitch=(Amplitude=350.0,Frequency=40.0),Yaw=(Amplitude=350.0,Frequency=30.0),Roll=(Amplitude=350.0,Frequency=60.0))
    object end
    // Reference: CameraShake'Default__FoxAnimNotify_CameraShakeRadius.Shake0'
    Shake=Shake0
    MaxRadius=2000.0
}