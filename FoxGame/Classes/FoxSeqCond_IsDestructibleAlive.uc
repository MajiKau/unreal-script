/*******************************************************************************
 * FoxSeqCond_IsDestructibleAlive generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxSeqCond_IsDestructibleAlive extends SequenceCondition
    forcescriptorder(true)
    hidecategories(Object);

var() FoxDestructible DestructibleActor;

event Activated()
{
    local int OutputLinkIndex;

    OutputLinkIndex = (((DestructibleActor != none) && DestructibleActor.CurrentStage == 0) ? 0 : 1);
    ActivateOutputLink(OutputLinkIndex);
    super(SequenceOp).Activated();
    //return;    
}

defaultproperties
{
    OutputLinks(0)=(Links=none,LinkDesc="True",bHasImpulse=false,bDisabled=false,bDisabledPIE=false,LinkedOp=none,ActivateDelay=0.0,DrawY=0,bHidden=false,bMoving=false,bClampedMax=false,bClampedMin=false,OverrideDelta=0,PIEActivationTime=0.0,bIsActivated=false)
    OutputLinks(1)=(Links=none,LinkDesc="False",bHasImpulse=false,bDisabled=false,bDisabledPIE=false,LinkedOp=none,ActivateDelay=0.0,DrawY=0,bHidden=false,bMoving=false,bClampedMax=false,bClampedMin=false,OverrideDelta=0,PIEActivationTime=0.0,bIsActivated=false)
    VariableLinks(0)=(ExpectedType=Class'Engine.SeqVar_Object',LinkedVariables=none,LinkDesc="Destructible",LinkVar=None,PropertyName=DestructibleActor,bWriteable=false,bSequenceNeverReadsOnlyWritesToThisVar=false,bModifiesLinkedObject=false,bHidden=false,MinVars=1,MaxVars=255,DrawX=0,CachedProperty=none,bAllowAnyType=false,bMoving=false,bClampedMax=false,bClampedMin=false,OverrideDelta=0)
    ObjName="Is Destructible Alive"
}