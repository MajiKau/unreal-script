/*******************************************************************************
 * FoxCameraLensEffect_HRVJammerBase generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxCameraLensEffect_HRVJammerBase extends FoxCameraLensEffect_Base
    hidecategories(Navigation);

defaultproperties
{
    DistFromCamera=12.0
    begin object name=ParticleSystemComponent0 class=ParticleSystemComponent
        ReplacementPrimitive=none
    object end
    // Reference: ParticleSystemComponent'Default__FoxCameraLensEffect_HRVJammerBase.ParticleSystemComponent0'
    ParticleSystemComponent=ParticleSystemComponent0
    begin object name=ParticleSystemComponent0 class=ParticleSystemComponent
        ReplacementPrimitive=none
    object end
    // Reference: ParticleSystemComponent'Default__FoxCameraLensEffect_HRVJammerBase.ParticleSystemComponent0'
    Components(0)=ParticleSystemComponent0
    DrawScale=0.750
    LifeSpan=600.0
}