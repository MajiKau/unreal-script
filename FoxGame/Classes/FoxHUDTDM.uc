/*******************************************************************************
 * FoxHUDTDM generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxHUDTDM extends FoxHUD
    transient
    native(HUD)
    config(UI)
    hidecategories(Navigation);

var transient byte WinningTeamIndex;
var transient int Team1Score;
var transient int Team2Score;
var transient int Team1RoundsWon;
var transient int Team2RoundsWon;
var transient int Team1FinalRunningTotalScore;
var transient int Team2FinalRunningTotalScore;
var const localized string LocStringVictory;
var const localized string LocStringSubVictory;
var const localized string LocStringSubRoundVictory;
var const localized string LocStringDefeat;
var const localized string LocStringSubDefeat;
var const localized string LocStringSubRoundDefeat;
var const localized string LocStringMatchOver;
var const localized string LocStringSubMatchOver;
var const localized string LocStringYourTeamWins;
var const localized string LocStringSwitchingTeams;

function NotifyGameOver(byte InWinningTeamIndex, int GameEndExperience, int inTeam1Score, int inTeam2Score, int inTeam1RunningTotalScore, int inTeam2RunningTotalScore, int inTeam1RoundsWon, int inTeam2RoundsWon, int inFirstPlaceID, int inSecondPlaceID, int inThirdPlaceID)
{
    local bool bWeWon;
    local array<string> args;
    local FoxGRI GRI;

    // End:0x2D
    if(FoxPC(PlayerOwner).bExitingServer)
    {
        return;
    }
    GRI = FoxGRI(WorldInfo.GRI);
    WinningTeamIndex = InWinningTeamIndex;
    Team1Score = inTeam1Score;
    Team2Score = inTeam2Score;
    Team1FinalRunningTotalScore = inTeam1RunningTotalScore;
    Team2FinalRunningTotalScore = inTeam2RunningTotalScore;
    Team1RoundsWon = inTeam1RoundsWon;
    Team2RoundsWon = inTeam2RoundsWon;
    MatchEndXP = GameEndExperience;
    GRI.FirstPlaceID = inFirstPlaceID;
    GRI.SecondPlaceID = inSecondPlaceID;
    GRI.ThirdPlaceID = inThirdPlaceID;
    bWeWon = PlayerOwner.PlayerReplicationInfo.Team.TeamIndex == WinningTeamIndex;
    TransitionToEndGameResults();
    args.AddItem("DIA_HQ_GameAction");
    args.AddItem(FoxGRI(WorldInfo.GRI).GetAnnouncerName());
    args.AddItem(FoxGRI(WorldInfo.GRI).GameTypeCode);
    args.AddItem("Objective");
    args.AddItem(((bWeWon) ? "Completed" : "Failed"));
    PostAkDialog(args, true);
    //return;    
}

function BuildAARXPInfo()
{
    local FoxPRI MyPRI;
    local bool bWeWon;
    local FoxGRI GRI;
    local BreakdownData Data;

    GRI = FoxGRI(WorldInfo.GRI);
    MyPRI = FoxPRI(PCOwner.PlayerReplicationInfo);
    bWeWon = MyPRI.Team.TeamIndex == WinningTeamIndex;
    Data.sTitle = ((bWeWon) ? LocStringVictory : LocStringDefeat);
    Data.nAmount = int(float(MyPRI.MatchEndExperience) * GRI.PrivateMatchXPModifier);
    // End:0x195
    if(PCOwner.bHasSpawnedIn)
    {
        PostGameAARData.XPBreakdown.AddItem(Data);
    }
    super.BuildAARXPInfo();
    //return;    
}

function BuildAARGPInfo()
{
    local BreakdownData Data;
    local FoxPRI MyPRI;
    local int PerformanceGP, TimeGP, MatchEndGP;
    local bool bWeWon;
    local FoxGRI GRI;

    GRI = FoxGRI(WorldInfo.GRI);
    MyPRI = FoxPRI(PCOwner.PlayerReplicationInfo);
    bWeWon = MyPRI.Team.TeamIndex == WinningTeamIndex;
    // End:0x41F
    if(MyPRI.GetEarnedGP() > 0)
    {
        PerformanceGP = MyPRI.TotalKills * GameClass.default.GPEarnedPerKill;
        PerformanceGP = ((PerformanceGP > GameClass.default.MaxGPKillEarn) ? GameClass.default.MaxGPKillEarn : PerformanceGP);
        PerformanceGP *= GRI.PrivateMatchGPModifier;
        MatchEndGP = ((bWeWon) ? class<FoxGameMP_TDM>(GameClass).default.GPEarnedGameWin : class<FoxGameMP_TDM>(GameClass).default.GPEarnedGameLose);
        MatchEndGP *= GRI.PrivateMatchGPModifier;
        TimeGP = (MyPRI.GetEarnedGP() - PerformanceGP) - MatchEndGP;
        // End:0x2F5
        if(TimeGP > 0)
        {
            Data.nAmount = TimeGP;
            Data.sTitle = LocStringGPTime;
            PostGameAARData.GPBreakdown.AddItem(Data);
        }
        // End:0x391
        if(MatchEndGP > 0)
        {
            Data.nAmount = MatchEndGP;
            Data.sTitle = ((bWeWon) ? LocStringVictory : LocStringDefeat);
            PostGameAARData.GPBreakdown.AddItem(Data);
        }
        // End:0x415
        if(PerformanceGP > 0)
        {
            Data.nAmount = PerformanceGP;
            Data.sTitle = LocStringGPPerformance;
            PostGameAARData.GPBreakdown.AddItem(Data);
        }
        super.BuildAARGPInfo();
    }
    //return;    
}

function ShowEndGameResults(float XPos, float YPos)
{
    local string ResultText, SubString;
    local int OurScore, TheirScore, TotalNodes, OurNumCappedNodes, TheirNumCappedNodes;

    local float Duration;
    local FoxPRI MyPRI;
    local FoxGRI GRI;
    local string XPBonusString;
    local RoundInfoDisplay RoundDisplay;
    local string OurScoreDisplay, TheirScoreDisplay;

    MyPRI = FoxPRI(PCOwner.PlayerReplicationInfo);
    GRI = FoxGRI(WorldInfo.GRI);
    // End:0xB7
    if(MatchEndXP > 0)
    {
        XPBonusString = ("[+" @ string(int(float(MatchEndXP) * GRI.PrivateMatchXPModifier))) @ "XP]";
    }
    // End:0xED
    if(bOnlySpectator)
    {
        ResultText = LocStringMatchOver;
        SubString = LocStringSubMatchOver;
    }
    // End:0x1AF
    else
    {
        ResultText = ((MyPRI.Team.TeamIndex == WinningTeamIndex) ? LocStringVictory : LocStringDefeat);
        SubString = ((MyPRI.Team.TeamIndex == WinningTeamIndex) ? LocStringSubVictory : LocStringSubDefeat);
    }
    Duration = 4.0;
    // End:0x220
    if(MyPRI.Team.TeamIndex == 0)
    {
        OurScore = Team1Score;
        TheirScore = Team2Score;
    }
    // End:0x246
    else
    {
        OurScore = Team2Score;
        TheirScore = Team1Score;
    }
    // End:0x294
    if(!GRI.HasRounds())
    {
        OurScoreDisplay = string(OurScore);
        TheirScoreDisplay = string(TheirScore);
    }
    GetCapturedNodes(TotalNodes, OurNumCappedNodes, TheirNumCappedNodes);
    SetupHeroPose();
    RoundDisplay.roundText = LocStringRounds;
    RoundDisplay.additionalText = ((MyPRI.Team.TeamIndex == WinningTeamIndex) ? LocStringVictory : LocStringDefeat);
    RoundDisplay.roundPointsTotal = GetCurrentRoundMarkup();
    DynamicUI.as_showMatchResults(XPos, YPos, GameClass.default.GameTypeAbbreviatedName, ResultText, MyPRI.Team.TeamIndex == WinningTeamIndex, OurScoreDisplay, TheirScoreDisplay, TotalNodes, OurNumCappedNodes, TheirNumCappedNodes, SubString, Duration, XPBonusString, GRI.HasRounds(), RoundDisplay);
    //return;    
}

function OnShowTieBreaker()
{
    local GFxObject TieBreakerObject;
    local array<ASValue> args;
    local bool bShow;
    local FoxGRI GRI;

    GRI = FoxGRI(WorldInfo.GRI);
    // End:0x9D
    if(!GRI.HasRounds() || !GRI.bMatchIsOver)
    {
        bShow = Team1Score == Team2Score;
    }
    // End:0xBC
    else
    {
        bShow = Team1RoundsWon == Team2RoundsWon;
    }
    // End:0x2A2
    if(bShow)
    {
        args.Length = 1;
        args[0].Type = 5;
        args[0].B = PlayerOwner.PlayerReplicationInfo.Team.TeamIndex == WinningTeamIndex;
        TieBreakerObject = DynamicUI.HUD.ShowTieBreaker();
        // End:0x2A2
        if(TieBreakerObject != none)
        {
            DynamicUI.Advance(0.0);
            TieBreakerObject.SetString("tieBreakerLabel", class'FoxLobbyUIBase'.default.LocStringTieBreaker);
            TieBreakerObject.Invoke("blueTeamWin", args);
            TieBreakerObject.InvokeNoParams("transitionIn");
            J0x2A2:
        }
        // End:0x2A2
        else
        {
        }
    }
    //return;    
}

function GetCapturedNodes(out int TotalNodes, out int OurCappedNodes, out int TheirCappedNodes)
{
    //return;    
}

function ShowRoundEndUIForTeamGame(byte InWinningTeamIndex, int inTeam1Score, int inTeam2Score, int inTeam1RoundsWon, int inTeam2RoundsWon, bool bGameWillEnd)
{
    local string ResultText, SubString;
    local int OurScore, TheirScore, TotalNodes, OurNumCappedNodes, TheirNumCappedNodes;

    local float Duration;
    local FoxPRI MyPRI;
    local RoundInfoDisplay RoundDisplay;

    MyPRI = FoxPRI(PCOwner.PlayerReplicationInfo);
    // End:0x8A
    if(MyPRI.Team.TeamIndex == InWinningTeamIndex)
    {
        ResultText = LocStringRoundWon;
    }
    // End:0x9D
    else
    {
        ResultText = LocStringRoundLost;
    }
    // End:0xC0
    if(bGameWillEnd)
    {
        SubString = LocStringComputingWinner;
    }
    // End:0x11E
    else
    {
        SubString = ((FoxGRI_TDM(WorldInfo.GRI).ShouldTeamsSwap()) ? LocStringSwitchingTeams : LocStringWaitingForRound);
    }
    Duration = 5.0;
    // End:0x18F
    if(MyPRI.Team.TeamIndex == 0)
    {
        OurScore = inTeam1RoundsWon;
        TheirScore = inTeam2RoundsWon;
    }
    // End:0x1B5
    else
    {
        OurScore = inTeam2RoundsWon;
        TheirScore = inTeam1RoundsWon;
    }
    WinningTeamIndex = InWinningTeamIndex;
    Team1Score = inTeam1Score;
    Team2Score = inTeam2Score;
    GetCapturedNodes(TotalNodes, OurNumCappedNodes, TheirNumCappedNodes);
    RoundDisplay.roundText = LocStringRounds;
    RoundDisplay.roundPointsTotal = GetCurrentRoundMarkup();
    // End:0x2EB
    if(InWinningTeamIndex != 255)
    {
        RoundDisplay.additionalText = ((MyPRI.Team.TeamIndex == InWinningTeamIndex) ? LocStringSubRoundVictory : LocStringSubRoundDefeat);
    }
    // End:0x320
    else
    {
        // End:0x320
        if(!bGameWillEnd)
        {
            RoundDisplay.additionalText = LocStringWaitingForRound;
        }
    }
    // End:0x590
    if(DynamicUI != none)
    {
        // End:0x431
        if(DynamicUI.Menu == none)
        {
            DynamicUI.as_showMatchResults(0.0, 100.0, GameClass.default.GameTypeAbbreviatedName, ResultText, MyPRI.Team.TeamIndex == InWinningTeamIndex, string(OurScore), string(TheirScore), TotalNodes, OurNumCappedNodes, TheirNumCappedNodes, SubString, Duration,, true, RoundDisplay);
        }
        // End:0x590
        if(DynamicUI.HUD != none)
        {
            DynamicUI.HUD.InitializeSpectatorHud();
            DynamicUI.HUD.KillRespawnWidget();
            // End:0x50B
            if(bGameWillEnd)
            {
                DynamicUI.HUD.KillLoadoutSwapperWidget();
            }
            // End:0x590
            else
            {
                DynamicUI.HUD.BeginRoundEventTimer(GameClass.default.RoundTransitionTime, DynamicUI.HUD.LocStringRespawnTime);
            }
        }
    }
    SetTimer(5.0, false, 'ShowRoundEndScoreboard');
    SetTimer(GameClass.default.RoundTransitionTime, false, 'HideRoundEndScoreboard');
    //return;    
}

function HideRoundEndScoreboard()
{
    // End:0x6A
    if((DynamicUI != none) && DynamicUI.HUD != none)
    {
        DynamicUI.HUD.ToggleScoreBoard(false);
    }
    PCOwner.EnablePauseMenu();
    //return;    
}

function CleanupRoundEndIU()
{
    WinningTeamIndex = 255;
    Team1Score = 0;
    Team2Score = 0;
    Team1FinalRunningTotalScore = 0;
    Team2FinalRunningTotalScore = 0;
    super.CleanupRoundEndIU();
    //return;    
}

defaultproperties
{
    WinningTeamIndex=255
    LocStringVictory="VICTORY!"
    LocStringSubVictory="Your team has won the match"
    LocStringSubRoundVictory="Your team has won the round"
    LocStringDefeat="DEFEAT!"
    LocStringSubDefeat="Your team has lost the match"
    LocStringSubRoundDefeat="Your team has lost the round"
    LocStringMatchOver="MATCH OVER"
    LocStringYourTeamWins="YOUR TEAM WINS"
    LocStringSwitchingTeams="Switching Sides"
    GameClass=class'FoxGameMP_TDM'
    HudUIClass=class'FoxHudUI_TDM'
    LocStringRoundWon="ROUND WON!"
    LocStringRoundLost="ROUND LOST!"
}