/*******************************************************************************
 * FoxAnimNode_AimOffsetHolo generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxAnimNode_AimOffsetHolo extends FoxAnimNode_AimOffset
    native(Animation)
    hidecategories(Object,Object,Object,Object);

var Actor MeshOwner;
