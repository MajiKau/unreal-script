/*******************************************************************************
 * FoxHudMinigame_PlayerFreeze generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxHudMinigame_PlayerFreeze extends FoxHudMiniGameUse within GFxMoviePlayer
    native(HUD)
    config(HUD);

var byte LastReplicatedFreezeTime;
