/*******************************************************************************
 * FoxDamageType_Bullet_HollowPoint generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxDamageType_Bullet_HollowPoint extends FoxDamageType_Bullet_ArmorAdjusted;

defaultproperties
{
    BonusDamagePercent=-1.0
    KillStrings(0)="hollowed out %s"
    DamagedFFWaveform=ForceFeedbackWaveform'Default__FoxDamageType_Bullet_HollowPoint.ForceFeedbackWaveformDamage'
    KilledFFWaveform=ForceFeedbackWaveform'Default__FoxDamageType_Bullet_HollowPoint.ForceFeedbackWaveformDeath'
}