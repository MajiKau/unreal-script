/*******************************************************************************
 * FoxScoringInfoSND generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxScoringInfoSND extends FoxScoringInfoTDM
    config(Scoring);

var const config int ExpPickupBomb;
var const config int ExpPlantBomb;
var const config int ExpDefendFusedBomb;
var const config int ExpAttackBombTarget;
var const config int ExpKilledBombCarrier;
var const config int ExpDefuseBomb;

defaultproperties
{
    ExpPickupBomb=50
    ExpPlantBomb=200
    ExpDefendFusedBomb=100
    ExpAttackBombTarget=100
    ExpKilledBombCarrier=100
    ExpDefuseBomb=200
}