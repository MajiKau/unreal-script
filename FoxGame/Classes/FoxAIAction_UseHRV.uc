/*******************************************************************************
 * FoxAIAction_UseHRV generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxAIAction_UseHRV extends FoxAIHierarchicalAbility
    native(AI)
    editinlinenew
    hidecategories(Object);

// Export UFoxAIAction_UseHRV::execInitialize(FFrame&, void* const)
native function bool Initialize();

// Export UFoxAIAction_UseHRV::execGetLookTarget(FFrame&, void* const)
native function Vector GetLookTarget();

// Export UFoxAIAction_UseHRV::execGetRotationTarget(FFrame&, void* const)
native function Vector GetRotationTarget();

defaultproperties
{
    PreCondition=FoxAICondition_HRVReady'Default__FoxAIAction_UseHRV.HRVReady'
    begin object name=EnterHRV class=FoxAIAction_SpecialMove
        SpecialMoveStateName=SpecialMove_HRVOn
    object end
    // Reference: FoxAIAction_SpecialMove'Default__FoxAIAction_UseHRV.EnterHRV'
    SubActions(0)=EnterHRV
    begin object name=Sleep class=FoxAIAction_Sleep
        bRandomizeDuration=true
        Durationmax=3.0
    object end
    // Reference: FoxAIAction_Sleep'Default__FoxAIAction_UseHRV.Sleep'
    SubActions(1)=Sleep
    begin object name=ExitHRV class=FoxAIAction_SpecialMove
        SpecialMoveStateName=SpecialMove_HRVOff
    object end
    // Reference: FoxAIAction_SpecialMove'Default__FoxAIAction_UseHRV.ExitHRV'
    SubActions(2)=ExitHRV
}