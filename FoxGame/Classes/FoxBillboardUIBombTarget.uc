/*******************************************************************************
 * FoxBillboardUIBombTarget generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxBillboardUIBombTarget extends FoxBillboardUIObjectBase within GFxMoviePlayer
    native(HUD)
    config(HUD);

var FoxGameObject_BombTarget BombTarget;
var FoxTypes.EControlPointType PointType;

defaultproperties
{
    symbolname="billBoard_Stationary"
    bWantsTeamColor=true
    bSupportsOpacityChangeInTightAim=true
}