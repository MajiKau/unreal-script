/*******************************************************************************
 * FoxAIAction_ScanHorizontal generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxAIAction_ScanHorizontal extends FoxAIAction
    native(AI)
    editinlinenew
    hidecategories(Object);

var() float MaxLookRange;

// Export UFoxAIAction_ScanHorizontal::execInitialize(FFrame&, void* const)
native function bool Initialize();

// Export UFoxAIAction_ScanHorizontal::execTick(FFrame&, void* const)
native function Tick(float DeltaTime);
