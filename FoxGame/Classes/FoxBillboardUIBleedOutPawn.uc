/*******************************************************************************
 * FoxBillboardUIBleedOutPawn generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxBillboardUIBleedOutPawn extends FoxBillboardUIObjectBase within GFxMoviePlayer
    native(HUD)
    config(HUD);

var FoxPawn Pawn;
var float LastProgress;
var const string HasSyringeColor;
var const string DefaultSyringeColor;

event as_SetTeamColor(string NewTeamColor)
{
    super.as_SetTeamColor(NewTeamColor);
    //return;    
}

defaultproperties
{
    LastProgress=-1.0
    HasSyringeColor="0x00ffb4"
    DefaultSyringeColor="0xFFD0D0"
    symbolname="billBoard_bleedOut"
    bSupportsOpacityChangeInTightAim=true
}