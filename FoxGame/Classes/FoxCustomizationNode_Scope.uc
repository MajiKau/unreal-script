/*******************************************************************************
 * FoxCustomizationNode_Scope generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxCustomizationNode_Scope extends FoxCustomizationNode_ModBase within FoxArmoryUI
    config(UI);

function Initialize(optional bool bSelectEquipped)
{
    local array<string> WeaponModClassNames;
    local class<FoxWeaponModObjectBase> CurrentModClass;

    bSelectEquipped = true;
    super(FoxCustomizationNode_Base).Initialize(bSelectEquipped);
    // End:0x66
    if(bSelectEquipped)
    {
        Outer.CustomItemType = 6;
        Outer.ResetSelectedLoadout();
    }
    WeaponModClassNames = Outer.LoadoutInfo.ScopeModNames;
    CurrentModClass = ((Outer.SelectedCustomizationType == 0) ? Outer.SavedPrimaryWeaponInfo.WeaponScope : Outer.SavedSecondaryWeaponInfo.WeaponScope);
    SetupModCustomScroller(WeaponModClassNames, CurrentModClass, bSelectEquipped);
    //return;    
}
