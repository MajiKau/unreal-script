/*******************************************************************************
 * FoxLeaderboardWriteLMS generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxLeaderboardWriteLMS extends FoxLeaderboardWriteGameTypeBase;

function CopyAllStats(FoxPRI PRI)
{
    local int NumKills, NumDeaths;

    // End:0xC8
    if(!FoxGRI(PRI.WorldInfo.GRI).bMigratingHost)
    {
        switch(PRI.ScorePlacement)
        {
            // End:0x96
            case 0:
                SetIntStat(268436992, 1);
                // End:0xC8
                break;
            // End:0xAD
            case 1:
                SetIntStat(268436993, 1);
                // End:0xC8
                break;
            // End:0xC5
            case 2:
                SetIntStat(268436994, 1);
                // End:0xC8
                break;
            // End:0xFFFF
            default:
            }
            NumKills = (PRI.Kills - PRI.PreviousKills) + PRI.StoredKills;
            NumDeaths = (PRI.Deaths - PRI.PreviousDeaths) + PRI.StoredDeaths;
            SetIntStat(268436995, NumKills);
            SetIntStat(268436996, NumDeaths);
            super.CopyAllStats(PRI);
            //return;            
}

defaultproperties
{
    TimePlayedPropertyID=268436997
    Properties=/* Array type was not detected. */
    ViewIds=/* Array type was not detected. */
    ArbitratedViewIds=/* Array type was not detected. */
    StatNameToStatIdMapping=/* Array type was not detected. */
}