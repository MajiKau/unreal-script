/*******************************************************************************
 * FoxAccessControl generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxAccessControl extends AccessControl
    config(Game)
    hidecategories(Navigation,Movement,Collision);

function bool IsAdmin(PlayerController P)
{
    // End:0x32
    if(super.IsAdmin(P) || FoxAdmin(P) != none)
    {
        return true;
    }
    return false;
    //return ReturnValue;    
}

function string GetAdminPassword()
{
    return AdminPassword;
    //return ReturnValue;    
}

function bool KickPlayer(PlayerController C, string KickReason)
{
    // End:0x5B
    if((C != none) && NetConnection(C.Player) != none)
    {
        return ForceKickPlayer(C, KickReason);
    }
    return false;
    //return ReturnValue;    
}

function KickBan(string Target)
{
    local PlayerController P;

    P = PlayerController(GetControllerFromString(Target));
    // End:0x168
    if(NetConnection(P.Player) != none)
    {
        // End:0x14C
        if(P.PlayerReplicationInfo.UniqueId != P.PlayerReplicationInfo.default.UniqueId && !IsIDBanned(P.PlayerReplicationInfo.UniqueId))
        {
            BannedIDs.AddItem(P.PlayerReplicationInfo.UniqueId);
            SaveConfig();
        }
        KickPlayer(P, DefaultKickReason);
    }
    //return;    
}

defaultproperties
{
    AdminClass=class'FoxAdmin'
}