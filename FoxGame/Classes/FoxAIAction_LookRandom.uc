/*******************************************************************************
 * FoxAIAction_LookRandom generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxAIAction_LookRandom extends FoxAIAction
    native(AI)
    editinlinenew
    hidecategories(Object);

var() float MinYaw;
var() float MaxYaw;
var() float MinPitch;
var() float MaxPitch;
var() float MinRoll;
var() float MaxRoll;
var() float LookDistance;
var transient Vector LastLookTarget;
var transient Vector CurrentLookTarget;

// Export UFoxAIAction_LookRandom::execInitialize(FFrame&, void* const)
native function bool Initialize();

// Export UFoxAIAction_LookRandom::execCleanup(FFrame&, void* const)
native function Cleanup();

// Export UFoxAIAction_LookRandom::execTick(FFrame&, void* const)
native function Tick(float DeltaTime);

// Export UFoxAIAction_LookRandom::execProcessBehaviorEvent(FFrame&, void* const)
native function bool ProcessBehaviorEvent(string EventName, optional Actor EventInstigator, optional Actor EventRecipient, optional Vector EventLocation);

defaultproperties
{
    MinYaw=-0.330
    MaxYaw=0.330
    MinPitch=-0.070
    MaxPitch=0.250
    LookDistance=500.0
    Duration=4.0
}