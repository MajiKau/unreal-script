/*******************************************************************************
 * FoxPawn_CrewServedWeapon generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxPawn_CrewServedWeapon extends FoxVehicle
    abstract
    native(Vehicle)
    config(Pawn)
    hidecategories(Navigation);

var() class<FoxWeapon> DefaultWeaponClass;

simulated function PostBeginPlay()
{
    super.PostBeginPlay();
    AddInventory(DefaultWeaponClass, true);
    //return;    
}

simulated function Tick(float DeltaTime)
{
    super(Actor).Tick(DeltaTime);
    //return;    
}

simulated function ProcessViewRotation(float DeltaTime, out Rotator out_ViewRotation, out Rotator out_DeltaRot)
{
    local Rotator camera_clamp;
    local Vector camera_location;
    local Vector2D NewAimOffset;
    local float DelX, DelY;

    DelX = float(out_DeltaRot.Yaw) / 32768.0;
    DelY = float(out_DeltaRot.Pitch) / 32768.0;
    NewAimOffset.X = FClamp(AimOffsetPct.X + DelX, -1.0, 1.0);
    NewAimOffset.Y = FClamp(AimOffsetPct.Y + DelY, -1.0, 1.0);
    UpdateAimOffset(NewAimOffset, DeltaTime);
    // End:0x1C5
    if(PlayerController(Controller) != none)
    {
        GetActorEyesViewPoint(camera_location, camera_clamp);
        // End:0x1B2
        if((bFPCameraLocIsSocket == true) && FPCameraLocName != 'None')
        {
            Mesh.GetSocketWorldLocationAndRotation(FPCameraLocName, camera_location, camera_clamp);
        }
        out_ViewRotation = camera_clamp;
    }
    //return;    
}

event TakeDamage(int Damage, Controller EventInstigator, Vector HitLocation, Vector Momentum, class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{
    local int newHealth;

    newHealth = Health - Damage;
    // End:0x3A
    if(newHealth <= 0)
    {
        DriverLeave(true);
    }
    super(FoxPawn).TakeDamage(Damage, EventInstigator, HitLocation, Momentum, DamageType, HitInfo, DamageCauser);
    //return;    
}

function bool TryToDrive(Pawn P)
{
    // End:0x42
    if(super.TryToDrive(P))
    {
        FoxPawn(P).SetMeshVisibility(1);
        return true;
    }
    return false;
    //return ReturnValue;    
}

event bool DriverLeave(bool bForceLeave)
{
    // End:0x38
    if(super.DriverLeave(bForceLeave))
    {
        Driver.RestoreMeshVisibility();
        return true;
    }
    return false;
    //return ReturnValue;    
}

simulated function AttachDriver(Pawn P)
{
    // End:0x11
    if(!bAttachDriver)
    {
        return;
    }
    P.SetHardAttach(true);
    P.SetPhysics(0);
    // End:0xBD
    if((P.Mesh != none) && Mesh != none)
    {
        P.Mesh.SetShadowParent(Mesh);
    }
    // End:0x10D
    if(!bDriverIsVisible)
    {
        P.SetHidden(true);
        P.SetLocation(Location);
    }
    //return;    
}

simulated function FaceRotation(Rotator NewRotation, float DeltaTime)
{
    //return;    
}

function ThrowWeaponOnDeath()
{
    //return;    
}

defaultproperties
{
    bDriverIsVisible=true
    DriverDamageMult=1.0
    bUseWeaponAnimOverrides=false
    bFPCameraLocIsSocket=true
    bTPCameraLocIsSocket=true
    bCanBeLungeTarget=false
    bPawnIsAWeapon=true
    EnemyHVTMaterialInstance=MaterialInstanceConstant'Default__FoxPawn_CrewServedWeapon.EnemyHVTMaterialInstance0'
    JamEnemyHVTMaterialInstance=MaterialInstanceConstant'Default__FoxPawn_CrewServedWeapon.JamEnemyHVTMaterialInstance0'
    FriendlyHVTMaterialInstance=MaterialInstanceConstant'Default__FoxPawn_CrewServedWeapon.FriendlyHVTMaterialInstance0'
    NuetralHVTMaterialInstance=MaterialInstanceConstant'Default__FoxPawn_CrewServedWeapon.NuetralHVTMaterialInstance0'
    EnemyIRMaterialInstance=MaterialInstanceConstant'Default__FoxPawn_CrewServedWeapon.EnemyIRMaterialInstance0'
    FriendlyIRMaterialInstance=MaterialInstanceConstant'Default__FoxPawn_CrewServedWeapon.FriendlyIRMaterialInstance0'
    NuetralIRMaterialInstance=MaterialInstanceConstant'Default__FoxPawn_CrewServedWeapon.NuetralIRMaterialInstance0'
    LightEnvironment=DynamicLightEnvironmentComponent'Default__FoxPawn_CrewServedWeapon.TPLightEnvironment'
    TakeDamageCameraShake=CameraShake'Default__FoxPawn_CrewServedWeapon.Shake0'
    BlendedEyeHeight=256.0
    EyeHeightBlendAccel=64.0
    AimOffsetInterpSpeed=8.0
    FPCameraLocName=S_Camera_02
    TPCameraLocName=S_Camera_02
    FreeCamLivingBoneName=Pivot_01
    FreeCamDeadBoneName=Pivot_01
    FreeCamLivingBoneTranslation=(X=0.0,Y=0.0,Z=0.0)
    AimAttractors=none
    AttachSlots(0)=(SocketName=S_Muzzle,AttachClass=none,ThirdPersonWeapon=none,bVisible=false)
    AttachSlots(1)=(SocketName=LeftHand,AttachClass=none,ThirdPersonWeapon=none,bVisible=true)
    AttachSlots(2)=(SocketName=S_Primary,AttachClass=none,ThirdPersonWeapon=none,bVisible=false)
    AttachSlots(3)=(SocketName=S_Secondary,AttachClass=none,ThirdPersonWeapon=none,bVisible=false)
    AttachSlots(4)=(SocketName=S_Inv1,AttachClass=none,ThirdPersonWeapon=none,bVisible=false)
    AttachSlots(5)=(SocketName=S_Inv2,AttachClass=none,ThirdPersonWeapon=none,bVisible=false)
    AttachSlots(6)=(SocketName=S_Knife,AttachClass=none,ThirdPersonWeapon=none,bVisible=false)
    HealthRechargeDelay=50.0
    HealthRechargePercentPerSecond=0.0
    HealthThreshold=0.0
    DialogPawnName="CrewServed"
    DamageProfiles(0)=(TakeDamageCameraShake=CameraShake'Default__FoxPawn_CrewServedWeapon.Shake1',DamageAmount=40)
    DamageProfiles(1)=(TakeDamageCameraShake=CameraShake'Default__FoxPawn_CrewServedWeapon.Shake2',DamageAmount=150)
    DamageProfiles(2)=(TakeDamageCameraShake=CameraShake'Default__FoxPawn_CrewServedWeapon.Shake3',DamageAmount=1500)
    bCanCrouch=false
    bCanJump=false
    bCanWalk=false
    WalkingPhysics=EPhysics.PHYS_None
    BaseEyeHeight=256.0
    EyeHeight=256.0
    Health=500
    HealthMax=500
    LandMovementState=PlayerWalking
    begin object name=PawnSkeletalMeshComponent class=SkeletalMeshComponent
        ReplacementPrimitive=none
        LightEnvironment=DynamicLightEnvironmentComponent'Default__FoxPawn_CrewServedWeapon.TPLightEnvironment'
    object end
    // Reference: SkeletalMeshComponent'Default__FoxPawn_CrewServedWeapon.PawnSkeletalMeshComponent'
    Mesh=PawnSkeletalMeshComponent
    begin object name=CollisionCylinder class=CylinderComponent
        ReplacementPrimitive=none
    object end
    // Reference: CylinderComponent'Default__FoxPawn_CrewServedWeapon.CollisionCylinder'
    CylinderComponent=CollisionCylinder
    begin object name=CollisionCylinder class=CylinderComponent
        ReplacementPrimitive=none
    object end
    // Reference: CylinderComponent'Default__FoxPawn_CrewServedWeapon.CollisionCylinder'
    Components(0)=CollisionCylinder
    Components(1)=DynamicLightEnvironmentComponent'Default__FoxPawn_CrewServedWeapon.TPLightEnvironment'
    begin object name=PawnSkeletalMeshComponent class=SkeletalMeshComponent
        ReplacementPrimitive=none
        LightEnvironment=DynamicLightEnvironmentComponent'Default__FoxPawn_CrewServedWeapon.TPLightEnvironment'
    object end
    // Reference: SkeletalMeshComponent'Default__FoxPawn_CrewServedWeapon.PawnSkeletalMeshComponent'
    Components(2)=PawnSkeletalMeshComponent
    Components(3)=DynamicLightEnvironmentComponent'Default__FoxPawn_CrewServedWeapon.FPLightEnvironmentObj'
    begin object name=CollisionCylinder class=CylinderComponent
        ReplacementPrimitive=none
    object end
    // Reference: CylinderComponent'Default__FoxPawn_CrewServedWeapon.CollisionCylinder'
    CollisionComponent=CollisionCylinder
}