/*******************************************************************************
 * FoxSpecialMove_PlantBomb generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxSpecialMove_PlantBomb extends FoxSpecialMove_AnimationBase
    native(SpecialMoves);

enum EBombPlantState
{
    EBPS_Entry,
    EBPS_Idle,
    EBPS_Exit,
    EBPS_MAX
};

var FoxSpecialMove_PlantBomb.EBombPlantState BombPlantAnimState;

function bool CanChainMove(FoxTypes.ESpecialMove NextMove)
{
    // End:0x16
    if(NextMove == 33)
    {
        return true;
    }
    return false;
    //return ReturnValue;    
}

simulated function StartSpecialMove()
{
    BombPlantAnimState = 0;
    super.StartSpecialMove();
    //return;    
}

simulated function AnimEndNotification(AnimNodeSequence SeqNode, float PlayedTime, float ExcessTime)
{
    // End:0x90
    if(!PawnOwner.IsLocallyControlled())
    {
        PawnOwner.PlayBodyStance(Anims[1].TP_Anim, Anims[1].RateMultiplier, 0.0, 0.150, true);
    }
    // End:0xF8
    else
    {
        PawnOwner.PlayFirstPersonCustomAnim(Anims[1].FP_Anim, Anims[1].RateMultiplier, 0.0, 0.150, true, true);
    }
    BombPlantAnimState = 1;
    //return;    
}

protected simulated function bool GetAnimsToPlay(out WeaponAnimSet OutAnims)
{
    OutAnims = Anims[BombPlantAnimState];
    return true;
    //return ReturnValue;    
}

defaultproperties
{
    Anims(0)=(TP_Anim=(AnimName=(None,None,Arm_Entry_A)),FP_Anim=Arm_Entry_A,WP_Anim=Arm_Entry_A,HG_Anim=None,RateMultiplier=1.0,BlendInTime=0.250,BlendOutTime=0.250,AbortBlendOutTime=0.10)
    Anims(1)=(TP_Anim=(AnimName=(None,None,Arm_Loop_A)),FP_Anim=Arm_Loop_A,WP_Anim=Arm_Loop_A,HG_Anim=None,RateMultiplier=1.0,BlendInTime=0.250,BlendOutTime=0.250,AbortBlendOutTime=0.10)
    Anims(2)=(TP_Anim=(AnimName=(None,None,Arm_End_A)),FP_Anim=Arm_End_A,WP_Anim=Arm_End_A,HG_Anim=None,RateMultiplier=1.0,BlendInTime=0.250,BlendOutTime=0.250,AbortBlendOutTime=0.10)
    bUseRandomMotionVariation=false
    BlendOutSpeed=0.0
    bCanChangeCrouchState=true
    bLockPawnRotation=false
    bDisableMovement=false
    bDebug=true
}