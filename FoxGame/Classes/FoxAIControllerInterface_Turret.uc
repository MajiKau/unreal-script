/*******************************************************************************
 * FoxAIControllerInterface_Turret generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxAIControllerInterface_Turret extends FoxAIControllerInterface_Idler
    native(AI)
    hidecategories(Navigation,Look,Path,Error,Movement,Display,Attachment,Object,Advanced,Physics,Collision);

defaultproperties
{
    begin object name=SpawnIn class=FoxAIAction_Sleep
        Durationmax=1.70
        Duration=1.70
        Modifier=FoxAIModifier_ActivationCounter'Default__FoxAIControllerInterface_Turret.SpawnSleep'
    object end
    // Reference: FoxAIAction_Sleep'Default__FoxAIControllerInterface_Turret.SpawnIn'
    Behaviors(0)=SpawnIn
    begin object name=Attack class=FoxAIAction_Sequence
        PreCondition=FoxAICondition_HasEnemy'Default__FoxAIControllerInterface_Turret.HasEnemyTargetCondition'
        begin object name=FightStationary class=FoxAIAction_FightStationary
            begin object name=Fire class=FoxAIAction_FireWeapon
                bStopFiringIfNotAimedAtEnemy=false
                BurstPauseMin=0.0
                BurstPauseMax=0.0
                Duration=2.0
            object end
            // Reference: FoxAIAction_FireWeapon'Default__FoxAIControllerInterface_Turret.Attack.FightStationary.Fire'
            SubActions(0)=Fire
        object end
        // Reference: FoxAIAction_FightStationary'Default__FoxAIControllerInterface_Turret.Attack.FightStationary'
        SubActions(0)=FightStationary
    object end
    // Reference: FoxAIAction_Sequence'Default__FoxAIControllerInterface_Turret.Attack'
    Behaviors(1)=Attack
    begin object name=CT class=FoxAIAction_ChooseTarget
        TargetingInfo=Filters=(158,41),
/* Exception thrown while deserializing TargetingInfo
System.ArgumentException: Der angeforderte Wert "3D Mode_138018815" konnte nicht gefunden werden.
   bei System.Enum.TryParseEnum(Type enumType, String value, Boolean ignoreCase, EnumResult& parseResult)
   bei System.Enum.Parse(Type enumType, String value, Boolean ignoreCase)
   bei UELib.Core.UDefaultProperty.Deserialize()
   bei UELib.Core.UDefaultProperty.DeserializeDefaultPropertyValue(PropertyType type, DeserializeFlags& deserializeFlags) */
        Modifier=none
        SubActions=none
    object end
    // Reference: FoxAIAction_ChooseTarget'Default__FoxAIControllerInterface_Turret.CT'
    Behaviors(2)=CT
    DefaultBehavior=FoxAIAction_LookRandom'Default__FoxAIControllerInterface_Turret.Idle'
    begin object name=Commands class=FoxAIAction_Selection
        SubActions(0)=FoxAIAction_GoTo'Default__FoxAIControllerInterface_Turret.Commands.Goto'
        SubActions(1)=FoxAIAction_FollowController'Default__FoxAIControllerInterface_Turret.Commands.Follow'
        SubActions(2)=FoxAIAction_Flee'Default__FoxAIControllerInterface_Turret.Commands.Flee'
        SubActions(3)=FoxAIAction_Wander'Default__FoxAIControllerInterface_Turret.Commands.Wander'
        SubActions(4)=FoxAIAction_SpecialMove'Default__FoxAIControllerInterface_Turret.Commands.Enter'
    object end
    // Reference: FoxAIAction_Selection'Default__FoxAIControllerInterface_Turret.Commands'
    ScriptedBehavior=Commands
    EvaluationInterval=0.20
    AggressionRange=2500.0
}