/*******************************************************************************
 * FoxScoringInfo_OS generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxScoringInfo_OS extends FoxScoringInfo
    config(Scoring);

var config int ExpTeamWinEasy;
var config int ExpTeamWinMedium;
var config int ExpTeamWinHard;
var config int ExpTeamWinHardcore;
var config int ExpTeamLose;

defaultproperties
{
    ExpTeamWinEasy=1500
    ExpTeamWinMedium=2000
    ExpTeamWinHard=2500
    ExpTeamWinHardcore=3000
    ExpTeamLose=1000
    ExpKill=4
    ExpAssist=5
    ExpHeadshot=5
    ExpKillTurret=8
    ExpKillFromTurret=8
    ExpKillBot=12
    ExpKillFromBot=8
}