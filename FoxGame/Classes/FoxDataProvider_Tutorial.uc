/*******************************************************************************
 * FoxDataProvider_Tutorial generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxDataProvider_Tutorial extends UIResourceDataProvider
    transient
    native
    config(Tutorial)
    perobjectconfig
    hidecategories(Object,UIRoot);

struct native TutorialMessageInfo
{
    var init string sMessageTitle;
    var init string sMessageBody;

    structdefaultproperties
    {
        sMessageTitle=""
        sMessageBody=""
    }
};

var const config int PositionPreset;
var const config string ArrowDirection;
var const config name NextTutorial;
var const config array<config TutorialMessageInfo> TutorialSequence;
