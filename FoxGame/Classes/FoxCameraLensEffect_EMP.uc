/*******************************************************************************
 * FoxCameraLensEffect_EMP generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxCameraLensEffect_EMP extends FoxCameraLensEffect_Fading
    abstract
    hidecategories(Navigation);

var private const name EMPHRVParamName;

function SetStrength(const float NewStrength)
{
    super.SetStrength(NewStrength);
    FadeMIC.SetScalarParameterValue(EMPHRVParamName, ((FoxPC(BaseCamera.PCOwner).IsHRVActive()) ? 1.0 : 0.0));
    //return;    
}

defaultproperties
{
    EMPHRVParamName=HRV_on
    FadeParamName=PixelInterp
    FadeInTime=0.010
    begin object name=ParticleSystemComponent0 class=ParticleSystemComponent
        ReplacementPrimitive=none
    object end
    // Reference: ParticleSystemComponent'Default__FoxCameraLensEffect_EMP.ParticleSystemComponent0'
    ParticleSystemComponent=ParticleSystemComponent0
    begin object name=ParticleSystemComponent0 class=ParticleSystemComponent
        ReplacementPrimitive=none
    object end
    // Reference: ParticleSystemComponent'Default__FoxCameraLensEffect_EMP.ParticleSystemComponent0'
    Components(0)=ParticleSystemComponent0
}