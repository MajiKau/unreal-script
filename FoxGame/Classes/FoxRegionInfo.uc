/*******************************************************************************
 * FoxRegionInfo generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxRegionInfo extends Object
    native;

enum EGameRegionDesignation
{
    EGRD_Production,
    EGRD_Beta,
    EGRD_Staging,
    EGRD_Development,
    EGRD_MAX
};

struct native RegionListEntry
{
    var init string NameID;
    var int RegionID;
    var init array<init int> InvisibleFrom;
    var FoxRegionInfo.EGameRegionDesignation RegionType;
    var init string sServerName;
    var init string FriendlyDescription;
    var float PingStartTime;
    var float PingResponseTime;
    var bool AnsweredPing;
    var init string sStatus;
    var init string sQueue;
    var init string sWait;
    var bool bIsOnline;

    structdefaultproperties
    {
        NameID=""
        RegionID=0
        InvisibleFrom=none
        RegionType=EGameRegionDesignation.EGRD_Production
        sServerName=""
        FriendlyDescription=""
        PingStartTime=0.0
        PingResponseTime=0.0
        AnsweredPing=false
        sStatus=""
        sQueue=""
        sWait=""
        bIsOnline=false
    }
};

var const string LocalizationPackageName;
var const string NameLocalizationSectionName;
var const string DescriptionLocalizationSectionName;
var private const array<RegionListEntry> RegionList;

// Export UFoxRegionInfo::execGetRegionEntryFromID(FFrame&, void* const)
native static function bool GetRegionEntryFromID(Engine.OnlineSubsystemTypes.EGameRegion RegionID, out RegionListEntry OutInfo);

// Export UFoxRegionInfo::execGetAllRegionEntries(FFrame&, void* const)
native static function GetAllRegionEntries(out array<RegionListEntry> OutRegions, optional bool bFilterInvisible, optional Engine.OnlineSubsystemTypes.EGameRegion RegionID);

defaultproperties
{
    LocalizationPackageName="Regions"
    NameLocalizationSectionName="RegionName"
    DescriptionLocalizationSectionName="RegionDescription"
    RegionList(0)=(NameID="IAHLive",RegionID=116,InvisibleFrom=(0,1,2),RegionType=EGameRegionDesignation.EGRD_Production,sServerName="",FriendlyDescription="",PingStartTime=0.0,PingResponseTime=0.0,AnsweredPing=false,sStatus="",sQueue="",sWait="",bIsOnline=false)
    RegionList(1)=(NameID="USWest",RegionID=0,InvisibleFrom=(13,17,255,2),RegionType=EGameRegionDesignation.EGRD_Production,sServerName="",FriendlyDescription="",PingStartTime=0.0,PingResponseTime=0.0,AnsweredPing=false,sStatus="",sQueue="",sWait="",bIsOnline=false)
    RegionList(2)=(NameID="Europe",RegionID=2,InvisibleFrom=(13,17,255,0,1),RegionType=EGameRegionDesignation.EGRD_Production,sServerName="",FriendlyDescription="",PingStartTime=0.0,PingResponseTime=0.0,AnsweredPing=false,sStatus="",sQueue="",sWait="",bIsOnline=false)
    RegionList(3)=(NameID="Russia",RegionID=4,InvisibleFrom=(0,1,2,3,13,19,102,119,255),RegionType=EGameRegionDesignation.EGRD_Production,sServerName="",FriendlyDescription="",PingStartTime=0.0,PingResponseTime=0.0,AnsweredPing=false,sStatus="",sQueue="",sWait="",bIsOnline=false)
}