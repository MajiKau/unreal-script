/*******************************************************************************
 * FoxPayloadStopVolume generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxPayloadStopVolume extends PhysicsVolume
    hidecategories(Navigation,Object,Movement,Display);

var() repnotify transient bool PayloadCanPass;
var private transient FoxGameMP_PL GameMode;

simulated function ReplicatedEvent(name VarName)
{
    // End:0x21
    if(VarName == 'PayloadCanPass')
    {
        UpdatePayload();
    }
    //return;    
}

simulated event PostBeginPlay()
{
    GameMode = FoxGameMP_PL(WorldInfo.Game);
    PayloadCanPass = false;
    //return;    
}

event AllowPayloadToPass()
{
    PayloadCanPass = true;
    UpdatePayload();
    //return;    
}

function UpdatePayload()
{
    GameMode.UpdatePayloadMovement();
    //return;    
}

event PawnEnteredVolume(Pawn Other)
{
    // End:0x6B
    if((Role == ROLE_Authority) && GameMode.PawnIsPayloadObject(Other))
    {
        GameMode.SetCurrentVolume(self);
        UpdatePayload();
    }
    //return;    
}

event PawnLeavingVolume(Pawn Other)
{
    //return;    
}

defaultproperties
{
    begin object name=BrushComponent0 class=BrushComponent
        ReplacementPrimitive=none
    object end
    // Reference: BrushComponent'Default__FoxPayloadStopVolume.BrushComponent0'
    BrushComponent=BrushComponent0
    begin object name=BrushComponent0 class=BrushComponent
        ReplacementPrimitive=none
    object end
    // Reference: BrushComponent'Default__FoxPayloadStopVolume.BrushComponent0'
    Components(0)=BrushComponent0
    begin object name=BrushComponent0 class=BrushComponent
        ReplacementPrimitive=none
    object end
    // Reference: BrushComponent'Default__FoxPayloadStopVolume.BrushComponent0'
    CollisionComponent=BrushComponent0
}