/*******************************************************************************
 * FoxCustomizationNode_Taunt generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxCustomizationNode_Taunt extends FoxCustomizationNode_Base within FoxArmoryUI
    native(UI)
    config(UI);

var transient array<FoxDataProvider_Emote> TauntInfoArray;
var int CurrentTauntIndex;
var int NextTauntToPlay;

function Initialize(optional bool bSelectEquipped)
{
    local ScrollerListEntry NewScrollerListEntry;
    local int I;
    local FoxDataProvider_Emote CurrentEmote;

    bSelectEquipped = true;
    super.Initialize(bSelectEquipped);
    ScrollerEntries.Length = 0;
    TauntInfoArray.Length = 0;
    Outer.SelectedScrollerIndex = 0;
    Outer.SelectedCustomizationType = 8;
    Outer.UpdateCharacterGear(Outer.CachedGearSlot);
    // End:0x337
    if((Outer.CustomItemType >= 5) && Outer.CustomItemType <= 8)
    {
        CurrentTauntIndex = Outer.CustomItemType - 5;
        I = 0;
        J0x132:
        // End:0x337 [Loop If]
        if(I < class'FoxUnlockInfo'.default.TauntUnlocks.Length)
        {
            CurrentEmote = Outer.Outer.Outer.UnlockablesDataStore.GetEmoteFromUnlockID(class'FoxUnlockInfo'.default.TauntUnlocks[I].UnlockID);
            // End:0x214
            if(CurrentEmote == none)
            {
            }
            // End:0x329
            else
            {
                // End:0x329
                if(SetScrollerEntry(NewScrollerListEntry, class'FoxUnlockInfo'.default.TauntUnlocks[I].UnlockID, bSelectEquipped))
                {
                    NewScrollerListEntry.InsertIndex = TauntInfoArray.Length;
                    // End:0x2FD
                    if(NewScrollerListEntry.UnlockID == Outer.CachedTauntSlots[CurrentTauntIndex])
                    {
                        SetEquipped(NewScrollerListEntry, bSelectEquipped);
                    }
                    ScrollerEntries.AddItem(NewScrollerListEntry);
                    TauntInfoArray.AddItem(CurrentEmote);
                }
            }
            ++ I;
            // [Loop Continue]
            goto J0x132;
        }
    }
    EquippedUnlockID = Outer.CachedTauntSlots[CurrentTauntIndex];
    SetupScrollerBar(bSelectEquipped);
    //return;    
}

function BuildComponentSlots()
{
    //return;    
}

function ei_changeScrollerIndex(int Index)
{
    NextTauntToPlay = Index;
    // End:0x9F
    if(FoxCustomPlayerActor(Outer.PreviewActor) != none)
    {
        Outer.Outer.Outer.SetTimer(0.50, TimedPlayTauntAnim);
    }
    super.ei_changeScrollerIndex(Index);
    //return;    
}

function ei_ScrollerClicked(string SelectedIndex)
{
    local int NextIndex;

    NextIndex = int(SelectedIndex);
    NextTauntToPlay = NextIndex;
    // End:0xB4
    if(FoxCustomPlayerActor(Outer.PreviewActor) != none)
    {
        Outer.Outer.Outer.SetTimer(0.50, TimedPlayTauntAnim);
    }
    super.ei_ScrollerClicked(SelectedIndex);
    //return;    
}

function NodeEquipButtonClicked(string SelectedIndex)
{
    local int NextIndex;

    NextIndex = int(SelectedIndex);
    // End:0x105
    if((0 <= NextIndex) && NextIndex < ScrollerEntries.Length)
    {
        Outer.CachedTauntSlots[CurrentTauntIndex] = ScrollerEntries[int(SelectedIndex)].UnlockID;
        Outer.Outer.ArmoryItemGrid.as_SetSelectedModIndex(Outer.SetModDataForTaunts(5 + CurrentTauntIndex), true);
    }
    Outer.SaveGearInfo();
    super.NodeEquipButtonClicked(SelectedIndex);
    //return;    
}

function TimedPlayTauntAnim()
{
    // End:0x1AE
    if(FoxCustomPlayerActor(Outer.PreviewActor) != none)
    {
        Outer.CurrentTauntPlaying = TauntInfoArray[ScrollerEntries[NextTauntToPlay].InsertIndex];
        // End:0x1AE
        if(Outer.CurrentTauntPlaying != none)
        {
            FoxCustomPlayerActor(Outer.PreviewActor).StartFullBodyAnimation(Outer.CurrentTauntPlaying.ThirdPersonAnimName, 0.250, false, true, !Outer.CurrentTauntPlaying.bHideWeapon);
            FoxCustomPlayerActor(Outer.PreviewActor).StartMeshEffects(Outer.CurrentTauntPlaying);
            J0x1AE:
        }
        // End:0x1AE
        else
        {
        }
    }
    //return;    
}

function Cleanup()
{
    Outer.Outer.Outer.ClearTimer(TimedPlayTauntAnim);
    super.Cleanup();
    //return;    
}

function bool IsPurchased(int ItemId)
{
    return FoxServerConnection(Outer.Outer.Outer.PCOwner.WorldInfo.ServerConn).IsTauntPurchased(ItemId, FoxPRI(Outer.Outer.Outer.PCOwner.PlayerReplicationInfo));
    //return ReturnValue;    
}

defaultproperties
{
    bComponentSlots=true
}