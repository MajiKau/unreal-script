/*******************************************************************************
 * FoxSeqAct_EndGame generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxSeqAct_EndGame extends SequenceAction
    forcescriptorder(true)
    hidecategories(Object);

event Activated()
{
    local WorldInfo WI;

    WI = GetWorldInfo();
    // End:0x95
    if((WI != none) && WI.Game != none)
    {
        WI.Game.EndGame(none, "Triggered from Kismet");
    }
    //return;    
}

defaultproperties
{
    VariableLinks=none
    ObjName="Trigger End Game"
}