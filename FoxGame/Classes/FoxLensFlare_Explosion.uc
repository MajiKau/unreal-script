/*******************************************************************************
 * FoxLensFlare_Explosion generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxLensFlare_Explosion extends LensFlareSource
    abstract
    native
    hidecategories(Navigation);

enum ELensExplosionState
{
    ELES_Stage1,
    ELES_Stage2,
    ELES_MAX
};

var protected const name OpacityParamName;
var protected const float Stage1ValueStart;
var protected const float Stage1ValueEnd;
var protected const float Stage2ValueEnd;
var protected const float Stage1TimeEnd;
var protected const float Stage2TimeEnd;
var private transient FoxLensFlare_Explosion.ELensExplosionState CurrentState;
var private transient float StateElapsedTime;
var protected export editinline MaterialInstanceConstant LensFlareMIC;

simulated function PostBeginPlay()
{
    super.PostBeginPlay();
    ApplyMaterials();
    //return;    
}

// Export UFoxLensFlare_Explosion::execApplyMaterials(FFrame&, void* const)
native simulated function ApplyMaterials();

defaultproperties
{
    OpacityParamName=Opacity
    Stage1ValueEnd=1.0
    Stage1TimeEnd=0.070
    Stage2TimeEnd=0.430
    begin object name=LensFlareComponent0 class=LensFlareComponent
        NextTraceTime=-0.42982270
        ReplacementPrimitive=none
        ViewOwnerDepthPriorityGroup=ESceneDepthPriorityGroup.SDPG_World
    object end
    // Reference: LensFlareComponent'Default__FoxLensFlare_Explosion.LensFlareComponent0'
    LensFlareComp=LensFlareComponent0
    Components(0)=none
    Components(1)=none
    Components(2)=none
    Components(3)=none
    begin object name=LensFlareComponent0 class=LensFlareComponent
        NextTraceTime=-0.42982270
        ReplacementPrimitive=none
        ViewOwnerDepthPriorityGroup=ESceneDepthPriorityGroup.SDPG_World
    object end
    // Reference: LensFlareComponent'Default__FoxLensFlare_Explosion.LensFlareComponent0'
    Components(4)=LensFlareComponent0
    Components(5)=none
    bNoDelete=false
}