/*******************************************************************************
 * ParticleModuleWorldAttractor generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class ParticleModuleWorldAttractor extends ParticleModuleWorldForcesBase
    native(Particle)
    editinlinenew
    hidecategories(Object,Object,Object);

var() bool bParticleLifeRelative;
var() RawDistributionFloat AttractorInfluence;

defaultproperties
{
    AttractorInfluence=Distribution=DistributionFloatConstant'Default__ParticleModuleWorldAttractor.DistributionInfluence',Type=0,Op=1,LookupTableNumElements=1,LookupTableChunkSize=1,LookupTable=/* Array type was not detected. */,
/* Exception thrown while deserializing AttractorInfluence
System.ArgumentOutOfRangeException: Der Index lag au?erhalb des Bereichs. Er darf nicht negativ und kleiner als die Sammlung sein.
Parametername: index
   bei System.ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument argument, ExceptionResource resource)
   bei UELib.UName.Deserialize(IUnrealStream stream)
   bei UELib.UObjectStream.ReadNameReference()
   bei UELib.Core.UDefaultProperty.Deserialize()
   bei UELib.Core.UDefaultProperty.DeserializeDefaultPropertyValue(PropertyType type, DeserializeFlags& deserializeFlags) */
    bUpdateModule=true
}