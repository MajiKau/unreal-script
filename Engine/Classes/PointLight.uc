/*******************************************************************************
 * PointLight generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class PointLight extends Light
    native(Light)
    placeable
    hidecategories(Navigation)
    classgroup(Lights,PointLights);

defaultproperties
{
    begin object name=PointLightComponent0 class=PointLightComponent
        CastDynamicShadows=false
        UseDirectLightMap=true
        LightingChannels=(Dynamic=false)
        LightAffectsClassification=ELightAffectsClassification.LAC_STATIC_AFFECTING
    object end
    // Reference: PointLightComponent'Default__PointLight.PointLightComponent0'
    LightComponent=PointLightComponent0
    Components(0)=none
    Components(1)=none
    Components(2)=none
    begin object name=PointLightComponent0 class=PointLightComponent
        CastDynamicShadows=false
        UseDirectLightMap=true
        LightingChannels=(Dynamic=false)
        LightAffectsClassification=ELightAffectsClassification.LAC_STATIC_AFFECTING
    object end
    // Reference: PointLightComponent'Default__PointLight.PointLightComponent0'
    Components(3)=PointLightComponent0
}