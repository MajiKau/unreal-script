/*******************************************************************************
 * MaterialExpressionParameter generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class MaterialExpressionParameter extends MaterialExpression
    native(Material)
    collapsecategories
    hidecategories(Object,Object);

var() name ParameterName;
var const Guid ExpressionGUID;
var() name Group;

defaultproperties
{
    bIsParameterExpression=true
    MenuCategories(0)=Parameters
}