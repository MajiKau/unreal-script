/*******************************************************************************
 * MaterialFunction generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class MaterialFunction extends Object
    native(Material)
    hidecategories(Object);

var duplicatetransient Guid StateId;
var editoronly transient MaterialFunction ParentFunction;
var() string Description;
var() bool bExposeToLibrary;
var private const transient bool bReentrantFlag;
var() array<string> LibraryCategories;
var array<MaterialExpression> FunctionExpressions;
var editoronly array<editoronly MaterialExpressionComment> FunctionEditorComments;

defaultproperties
{
    LibraryCategories(0)="Misc"
}