/*******************************************************************************
 * DominantDirectionalLight generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class DominantDirectionalLight extends DirectionalLight
    native(Light)
    placeable
    hidecategories(Navigation)
    classgroup(Lights,DirectionalLights);

defaultproperties
{
    LightComponent=DominantDirectionalLightComponent'Default__DominantDirectionalLight.DominantDirectionalLightComponent0'
    Components(0)=none
    Components(1)=none
    Components(2)=DominantDirectionalLightComponent'Default__DominantDirectionalLight.DominantDirectionalLightComponent0'
    bStatic=false
    bHardAttach=true
}