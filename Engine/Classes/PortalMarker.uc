/*******************************************************************************
 * PortalMarker generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class PortalMarker extends NavigationPoint
    native
    notplaceable
    hidecategories(Navigation,Lighting,LightColor,Force);

var PortalTeleporter MyPortal;

// Export UPortalMarker::execCanTeleport(FFrame&, void* const)
native function bool CanTeleport(Actor A);

defaultproperties
{
    begin object name=CollisionCylinder class=CylinderComponent
        ReplacementPrimitive=none
    object end
    // Reference: CylinderComponent'Default__PortalMarker.CollisionCylinder'
    CylinderComponent=CollisionCylinder
    Components(0)=none
    Components(1)=none
    Components(2)=none
    begin object name=CollisionCylinder class=CylinderComponent
        ReplacementPrimitive=none
    object end
    // Reference: CylinderComponent'Default__PortalMarker.CollisionCylinder'
    Components(3)=CollisionCylinder
    Components(4)=none
    bCollideWhenPlacing=false
    bHiddenEd=true
    begin object name=CollisionCylinder class=CylinderComponent
        ReplacementPrimitive=none
    object end
    // Reference: CylinderComponent'Default__PortalMarker.CollisionCylinder'
    CollisionComponent=CollisionCylinder
}