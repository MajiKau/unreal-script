/*******************************************************************************
 * OnlineGameSettings generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class OnlineGameSettings extends Settings
    native;

var databinding int NumOpenSlots;
var const QWord ServerNonce;
var string ServiceConfigId;
var string SessionTemplateName;
var string MatchHopperName;
var string SessionGuid;
var string SessionHandle;
var array<UniqueNetId> ReservedMembers;
var databinding bool bShouldAdvertise;
var databinding bool bIsLanMatch;
var databinding bool bUsesStats;
var databinding bool bAllowJoinInProgress;
var databinding bool bAllowInvites;
var databinding bool bUsesPresence;
var databinding bool bAllowJoinViaPresence;
var databinding bool bAllowJoinViaPresenceFriendsOnly;
var databinding bool bUsesArbitration;
var databinding bool bAntiCheatProtected;
var const bool bWasFromInvite;
var databinding bool bIsDedicated;
var const bool bHasSkillUpdateInProgress;
var const bool bShouldShrinkArbitratedSessions;
var const bool bJoinableForTrialUsers;
var bool bServerOffline;
var bool bServerExpired;
var bool bIsRankedMatch;
var bool bIsCoOpMatch;
var databinding string OwningPlayerName;
var UniqueNetId OwningPlayerId;
var databinding int PingInMs;
var databinding float MatchQuality;
var databinding OnlineSubsystem.EOnlineGameState GameState;
var databinding const int GamePort;
var const int BuildUniqueId;
var int InviteGameType;

event string GetPlaylistFriendlyName()
{
    return "";
    //return ReturnValue;    
}

event string GetPlaylistFriendlyDescription()
{
    return "";
    //return ReturnValue;    
}

event bool GetPlaylistPlayGoAvailable()
{
    return false;
    //return ReturnValue;    
}

event InitializeOnMigration()
{
    //return;    
}

defaultproperties
{
    bShouldAdvertise=true
    bUsesStats=true
    bAllowJoinInProgress=true
    bAllowInvites=true
    bUsesPresence=true
    bAllowJoinViaPresence=true
    bShouldShrinkArbitratedSessions=true
}