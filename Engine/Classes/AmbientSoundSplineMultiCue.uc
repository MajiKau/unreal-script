/*******************************************************************************
 * AmbientSoundSplineMultiCue generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class AmbientSoundSplineMultiCue extends AmbientSoundSpline
    native(Sound)
    placeable
    hidecategories(Navigation)
    autoexpandcategories(Audio,AmbientSoundSpline);

var(AmbientSoundSpline) editoronly int EditedSlot;

defaultproperties
{
    begin object name=AudioComponent2 class=MultiCueSplineAudioComponent
        bStopWhenOwnerDestroyed=true
        bShouldRemainActiveIfDropped=true
    object end
    // Reference: MultiCueSplineAudioComponent'Default__AmbientSoundSplineMultiCue.AudioComponent2'
    AudioComponent=AudioComponent2
    Components(0)=none
    Components(1)=none
    begin object name=SplineComponent0 class=SplineComponentSimplified
        ReplacementPrimitive=none
    object end
    // Reference: SplineComponentSimplified'Default__AmbientSoundSplineMultiCue.SplineComponent0'
    Components(2)=SplineComponent0
    begin object name=AudioComponent2 class=MultiCueSplineAudioComponent
        bStopWhenOwnerDestroyed=true
        bShouldRemainActiveIfDropped=true
    object end
    // Reference: MultiCueSplineAudioComponent'Default__AmbientSoundSplineMultiCue.AudioComponent2'
    Components(3)=AudioComponent2
}