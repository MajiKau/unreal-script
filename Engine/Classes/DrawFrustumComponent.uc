/*******************************************************************************
 * DrawFrustumComponent generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class DrawFrustumComponent extends PrimitiveComponent
    native
    editinlinenew
    collapsecategories
    noexport
    hidecategories(Object);

var() Color FrustumColor;
var() float FrustumAngle;
var() float FrustumAspectRatio;
var() float FrustumStartDist;
var() float FrustumEndDist;
var() Texture Texture;

defaultproperties
{
    FrustumColor=(R=255,G=0,B=255,A=255)
    FrustumAngle=90.0
    FrustumAspectRatio=1.333330
    FrustumStartDist=100.0
    FrustumEndDist=1000.0
    ReplacementPrimitive=none
    HiddenGame=true
}