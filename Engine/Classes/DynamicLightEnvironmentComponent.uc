/*******************************************************************************
 * DynamicLightEnvironmentComponent generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class DynamicLightEnvironmentComponent extends LightEnvironmentComponent
    native(Light);

enum EDynamicLightEnvironmentBoundsMethod
{
    DLEB_OwnerComponents,
    DLEB_ManualOverride,
    DLEB_ActiveComponents,
    DLEB_MAX
};

var private native const transient Pointer State;
var() float InvisibleUpdateTime;
var() float MinTimeBetweenFullUpdates;
var float VelocityUpdateTimeScale;
var float ShadowInterpolationSpeed;
var() int NumVolumeVisibilitySamples;
var() float LightingBoundsScale;
var LinearColor AmbientShadowColor;
var Vector AmbientShadowSourceDirection;
var LinearColor AmbientGlow;
var float LightDistance;
var float ShadowDistance;
var() bool bCastShadows;
var bool bCompositeShadowsFromDynamicLights;
var bool bForceCompositeAllLights;
var bool bAffectedBySmallDynamicLights;
var() bool bUseBooleanEnvironmentShadowing;
var bool bShadowFromEnvironment;
var() bool bDynamic;
var bool bSynthesizeDirectionalLight;
var() bool bSynthesizeSHLight;
var() bool bRequiresNonLatentUpdates;
var bool bTraceFromClosestBoundsPoint;
var() bool bIsCharacterLightEnvironment;
var bool bOverrideOwnerLightingChannels;
var bool bAlwaysInfluencedByDominantDirectionalLight;
var float ModShadowFadeoutTime;
var float ModShadowFadeoutExponent;
var LinearColor MaxModulatedShadowColor;
var float DominantShadowTransitionStartDistance;
var float DominantShadowTransitionEndDistance;
var float MinShadowAngle;
var DynamicLightEnvironmentComponent.EDynamicLightEnvironmentBoundsMethod BoundsMethod;
var BoxSphereBounds OverriddenBounds;
var LightingChannelContainer OverriddenLightingChannels;
var const export editinline array<export editinline LightComponent> OverriddenLightComponents;

// Export UDynamicLightEnvironmentComponent::execResetEnvironment(FFrame&, void* const)
native final function ResetEnvironment();

defaultproperties
{
    InvisibleUpdateTime=5.0
    MinTimeBetweenFullUpdates=1.0
    VelocityUpdateTimeScale=0.0000010
    ShadowInterpolationSpeed=0.0040
    NumVolumeVisibilitySamples=1
    LightingBoundsScale=1.0
    AmbientShadowColor=(R=0.0010,G=0.0010,B=0.0010,A=1.0)
    AmbientShadowSourceDirection=(X=0.010,Y=0.0,Z=0.990)
    AmbientGlow=(R=0.0,G=0.0,B=0.0,A=1.0)
    LightDistance=10.0
    ShadowDistance=5.0
    bCastShadows=true
    bCompositeShadowsFromDynamicLights=true
    bAffectedBySmallDynamicLights=true
    bUseBooleanEnvironmentShadowing=true
    bShadowFromEnvironment=true
    bDynamic=true
    bSynthesizeDirectionalLight=true
    ModShadowFadeoutExponent=3.0
    MaxModulatedShadowColor=(R=0.50,G=0.50,B=0.50,A=1.0)
    DominantShadowTransitionStartDistance=100.0
    DominantShadowTransitionEndDistance=10.0
    MinShadowAngle=25.0
}