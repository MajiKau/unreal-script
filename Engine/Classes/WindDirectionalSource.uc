/*******************************************************************************
 * WindDirectionalSource generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class WindDirectionalSource extends Info
    placeable
    hidecategories(Navigation,Movement,Collision)
    classgroup(Wind);

var() const editconst export editinline WindDirectionalSourceComponent Component;

defaultproperties
{
    Component=WindDirectionalSourceComponent'Default__WindDirectionalSource.WindDirectionalSourceComponent0'
    Components(0)=none
    Components(1)=WindDirectionalSourceComponent'Default__WindDirectionalSource.WindDirectionalSourceComponent0'
    Components(2)=none
    bNoDelete=true
}