/*******************************************************************************
 * ParticleModuleMeshMaterial generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class ParticleModuleMeshMaterial extends ParticleModuleMaterialBase
    native(Particle)
    editinlinenew
    hidecategories(Object,Object,Object);

var(MeshMaterials) array<MaterialInterface> MeshMaterials;

defaultproperties
{
    bSpawnModule=true
    bUpdateModule=true
}