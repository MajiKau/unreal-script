/*******************************************************************************
 * SpeedTreeComponent generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class SpeedTreeComponent extends PrimitiveComponent
    native(SpeedTree)
    editinlinenew
    hidecategories(Object)
    autoexpandcategories(Collision,Rendering,Lighting);

enum ESpeedTreeMeshType
{
    STMT_MinMinusOne,
    STMT_Branches1,
    STMT_Branches2,
    STMT_Fronds,
    STMT_LeafCards,
    STMT_LeafMeshes,
    STMT_Billboards,
    STMT_Max
};

struct native SpeedTreeStaticLight
{
    var private const Guid Guid;
    var private const ShadowMap1D BranchShadowMap;
    var private const ShadowMap1D FrondShadowMap;
    var private const ShadowMap1D LeafMeshShadowMap;
    var private const ShadowMap1D LeafCardShadowMap;
    var private const ShadowMap1D BillboardShadowMap;

    structdefaultproperties
    {
        Guid=(A=0,B=0,C=0,D=0)
        BranchShadowMap=none
        FrondShadowMap=none
        LeafMeshShadowMap=none
        LeafCardShadowMap=none
        BillboardShadowMap=none
    }
};

var(SpeedTree) const SpeedTree SpeedTree;
var(SpeedTree) bool bUseLeafCards;
var(SpeedTree) bool bUseLeafMeshes;
var(SpeedTree) bool bUseBranches;
var(SpeedTree) bool bUseFronds;
var(SpeedTree) bool bUseBillboards;
var(SpeedTree) float Lod3DStart;
var(SpeedTree) float Lod3DEnd;
var(SpeedTree) float LodBillboardStart;
var(SpeedTree) float LodBillboardEnd;
var(SpeedTree) float LodLevelOverride;
var(SpeedTree) MaterialInterface Branch1Material;
var(SpeedTree) MaterialInterface Branch2Material;
var(SpeedTree) MaterialInterface FrondMaterial;
var(SpeedTree) MaterialInterface LeafCardMaterial;
var(SpeedTree) MaterialInterface LeafMeshMaterial;
var(SpeedTree) MaterialInterface BillboardMaterial;
var private editoronly Texture2D SpeedTreeIcon;
var private const array<SpeedTreeStaticLight> StaticLights;
var private native const LightMapRef BranchLightMap;
var private native const LightMapRef FrondLightMap;
var private native const LightMapRef LeafMeshLightMap;
var private native const LightMapRef LeafCardLightMap;
var private native const LightMapRef BillboardLightMap;
var private native const Matrix RotationOnlyMatrix;
var(Lightmass) LightmassPrimitiveSettings LightmassSettings;

// Export USpeedTreeComponent::execGetMaterial(FFrame&, void* const)
native function MaterialInterface GetMaterial(SpeedTreeComponent.ESpeedTreeMeshType MeshType);

// Export USpeedTreeComponent::execSetMaterial(FFrame&, void* const)
native function SetMaterial(SpeedTreeComponent.ESpeedTreeMeshType MeshType, MaterialInterface Material);

defaultproperties
{
    bUseLeafCards=true
    bUseLeafMeshes=true
    bUseBranches=true
    bUseFronds=true
    bUseBillboards=true
    Lod3DStart=500.0
    Lod3DEnd=3000.0
    LodBillboardStart=3500.0
    LodBillboardEnd=4000.0
    LodLevelOverride=1.0
    LightmassSettings=(bUseTwoSidedLighting=false,bShadowIndirectOnly=false,bUseEmissiveForStaticLighting=false,EmissiveLightFalloffExponent=2.0,EmissiveLightExplicitInfluenceRadius=0.0,EmissiveBoost=1.0,DiffuseBoost=1.0,SpecularBoost=1.0,FullyOccludedSamplesFraction=1.0)
    ReplacementPrimitive=none
    bUseAsOccluder=true
    CastShadow=true
    bAcceptsLights=true
    bUsePrecomputedShadows=true
    CollideActors=true
    BlockActors=true
    BlockZeroExtent=true
    BlockNonZeroExtent=true
    BlockRigidBody=true
}