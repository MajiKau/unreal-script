/*******************************************************************************
 * GeneratedMeshAreaLight generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class GeneratedMeshAreaLight extends SpotLight
    native(Light)
    notplaceable
    hidecategories(Navigation);

defaultproperties
{
    begin object name=SpotLightComponent0 class=SpotLightComponent
        CastStaticShadows=false
        LightingChannels=(BSP=false,Static=false)
    object end
    // Reference: SpotLightComponent'Default__GeneratedMeshAreaLight.SpotLightComponent0'
    LightComponent=SpotLightComponent0
    Components(0)=none
    Components(1)=none
    Components(2)=none
    Components(3)=none
    Components(4)=none
    begin object name=SpotLightComponent0 class=SpotLightComponent
        CastStaticShadows=false
        LightingChannels=(BSP=false,Static=false)
    object end
    // Reference: SpotLightComponent'Default__GeneratedMeshAreaLight.SpotLightComponent0'
    Components(5)=SpotLightComponent0
    Components(6)=none
    bEditable=false
}