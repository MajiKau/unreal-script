/*******************************************************************************
 * PortalTeleporter generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class PortalTeleporter extends SceneCapturePortalActor
    abstract
    native
    notplaceable
    hidecategories(Navigation);

var() PortalTeleporter SisterPortal;
var() int TextureResolutionX;
var() int TextureResolutionY;
var PortalMarker MyMarker;
var() bool bMovablePortal;
var bool bAlwaysTeleportNonPawns;
var bool bCanTeleportVehicles;

// Export UPortalTeleporter::execTransformActor(FFrame&, void* const)
native final function bool TransformActor(Actor A);

// Export UPortalTeleporter::execTransformVectorDir(FFrame&, void* const)
native final function Vector TransformVectorDir(Vector V);

// Export UPortalTeleporter::execTransformHitLocation(FFrame&, void* const)
native final function Vector TransformHitLocation(Vector HitLocation);

// Export UPortalTeleporter::execCreatePortalTexture(FFrame&, void* const)
native final function TextureRenderTarget2D CreatePortalTexture();

simulated function bool StopsProjectile(Projectile P)
{
    return !TransformActor(P);
    //return ReturnValue;    
}

defaultproperties
{
    TextureResolutionX=256
    TextureResolutionY=256
    bAlwaysTeleportNonPawns=true
    begin object name=StaticMeshComponent2 class=StaticMeshComponent
        ReplacementPrimitive=none
        HiddenGame=false
        CollideActors=true
    object end
    // Reference: StaticMeshComponent'Default__PortalTeleporter.StaticMeshComponent2'
    StaticMesh=StaticMeshComponent2
    SceneCapture=SceneCapturePortalComponent'Default__PortalTeleporter.SceneCapturePortalComponent0'
    Components(0)=SceneCapturePortalComponent'Default__PortalTeleporter.SceneCapturePortalComponent0'
    Components(1)=none
    begin object name=StaticMeshComponent2 class=StaticMeshComponent
        ReplacementPrimitive=none
        HiddenGame=false
        CollideActors=true
    object end
    // Reference: StaticMeshComponent'Default__PortalTeleporter.StaticMeshComponent2'
    Components(2)=StaticMeshComponent2
    bWorldGeometry=true
    bMovable=false
    bCollideActors=true
    bBlockActors=true
    begin object name=StaticMeshComponent2 class=StaticMeshComponent
        ReplacementPrimitive=none
        HiddenGame=false
        CollideActors=true
    object end
    // Reference: StaticMeshComponent'Default__PortalTeleporter.StaticMeshComponent2'
    CollisionComponent=StaticMeshComponent2
}