/*******************************************************************************
 * ParticleModuleLocationEmitter generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class ParticleModuleLocationEmitter extends ParticleModuleLocationBase
    native(Particle)
    editinlinenew
    hidecategories(Object,Object,Object);

enum ELocationEmitterSelectionMethod
{
    ELESM_Random,
    ELESM_Sequential,
    ELESM_MAX
};

var(Location) noclear export name EmitterName;
var(Location) ParticleModuleLocationEmitter.ELocationEmitterSelectionMethod SelectionMethod;
var(Location) bool InheritSourceVelocity;
var(Location) bool bInheritSourceRotation;
var(Location) float InheritSourceVelocityScale;
var(Location) float InheritSourceRotationScale;

defaultproperties
{
    InheritSourceVelocityScale=1.0
    InheritSourceRotationScale=1.0
    bSpawnModule=true
}