/*******************************************************************************
 * ParticleModuleLocationStaticVertSurface generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class ParticleModuleLocationStaticVertSurface extends ParticleModuleLocationBase
    native(Particle)
    editinlinenew
    hidecategories(Object,Object,Object);

enum ELocationStaticVertSurfaceSource
{
    VERTSTATICSURFACESOURCE_Vert,
    VERTSTATICSURFACESOURCE_Surface,
    VERTSTATICSURFACESOURCE_MAX
};

var(VertSurface) ParticleModuleLocationStaticVertSurface.ELocationStaticVertSurfaceSource SourceType;
var(VertSurface) Vector UniversalOffset;
var(VertSurface) bool bUpdatePositionEachFrame;
var(VertSurface) bool bOrientMeshEmitters;
var(VertSurface) bool bEnforceNormalCheck;
var(VertSurface) name StaticMeshActorParamName;
var(VertSurface) editoronly StaticMesh EditorStaticMesh;
var(VertSurface) Vector NormalToCompare;
var(VertSurface) float NormalCheckToleranceDegrees;
var float NormalCheckTolerance;
var(VertSurface) array<int> ValidMaterialIndices;

defaultproperties
{
    bOrientMeshEmitters=true
    StaticMeshActorParamName=VertSurfaceActor
    bSpawnModule=true
    bUpdateModule=true
    bFinalUpdateModule=true
    bSupported3DDrawMode=true
}