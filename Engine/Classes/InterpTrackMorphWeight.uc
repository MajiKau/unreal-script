/*******************************************************************************
 * InterpTrackMorphWeight generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class InterpTrackMorphWeight extends InterpTrackFloatBase
    native(Interpolation)
    collapsecategories
    hidecategories(Object);

var() name MorphNodeName;

defaultproperties
{
    TrackInstClass=class'InterpTrackInstMorphWeight'
    TrackTitle="Morph Weight"
    bIsAnimControlTrack=true
}