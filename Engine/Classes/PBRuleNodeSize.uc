/*******************************************************************************
 * PBRuleNodeSize generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class PBRuleNodeSize extends PBRuleNodeBase
    native(ProcBuilding)
    editinlinenew
    collapsecategories
    hidecategories(Object,Object);

var() ProcBuildingRuleset.EProcBuildingAxis SizeAxis;
var() float DecisionSize;
var() bool bUseTopLevelScopeSize;

defaultproperties
{
    DecisionSize=512.0
    NextRules(0)=(NextRule=none,LinkName=Less,DrawY=0)
    NextRules(1)=(NextRule=none,LinkName=Greater/Equal,DrawY=0)
}